---
hide:
    - toc
---
![](../images/2PF/desarrollo.png)
# II. DESARROLLO DEL PROYECTO FINAL

En el desarrollo tenemos los diferentes items:

 * En la **PRIMERA PARTE** está relacionado al proceso de diseño,  a la Fabricación y a los materiales propuestos para la parte de Carpintería asi como los materiales para la parte electrónica.

* Como **SEGUNDA PARTE** tenemos la etapa de prototipado a escala pequeña y escala 1/1. La escala 1/1 es usada para prototipar la parte electrónica.

* En la  **TERCERA PARTE** elaboraremos el analisis de Costo Unitarios de los muebles así como el prototipado del Joint que se usará en la Fabricación de los muebles.

# 1. PRIMERA PARTE

  **I.) PLANOS DE ESCRITORIO.**

  * Para el desarrollo de los planos empezamos con un boceto a mano alzada y de ahi procedemos a dibujar los planos usando el programa de **AUTOCAD** y **RHINOCEROS**
  * El material a usar es **TRYPLAY FENÓLICO DE 18MM**, lo cual nos permite calcular una tolerancia para los encastres XY y el encastre dentado de 18.5mm. Usaremos para cortar una fresa de 6.35mm para lo cual usaremos los
**DOG BONES** de 7mm

  * El escritorio estará compuesto por 8 piezas:

      a.1. El tablero

      a.2. La Pata Lateral Derecha

      a.3. La Pata Lateral Izquierda

      a.4. La Pata Central

      a.5  La base del Escritorio

      a.6  Una pizarra esta colocada en la pata Central

      a.7  Unas asas que sostendrán a la pizarra

      a.8  Una pieza sirve para que haya tipo un cajón para que guarde cosas.

  * El Escritorio de dimensiones de: 600mm de largo x 600mm de ancho por 650mm de alto

![](../images/2PF/2.png)

  **I.) PLANOS DE LA SILLA**

  * He realizado un boceto a mano alzada y de ahi he dibujado los planos en el programa de **AUTOCAD** y **RHINOCEROS**
  * He tomado en cuenta el material a usar es **TRYPLAY FENÓLICO DE 15MM** para la silla, es de suma importancia para calcular la tolerancia en los encastres que es 15.5mm. Usaremos para cortar una fresa de 6.35mm para lo cual usaremos los **DOG BONES** de 7mm

  * La silla conformada por 6 piezas.

      b.1. El asiento

      b.2. Las piezas Laterales (2)

      b.3. La Pata Lateral Izquierda

      b.4. La pieza frontal posterior

      b.5  La pieza frontal inferior

  ![](../images/2PF/3.png)

  **II.) DISEÑO Y PROGRAMACION DE ARDUINO**

  Se detallará la programación que es realizada en **ARDUINO** para las tres piezas que llevarán elementos Electrónicos. Cada programacióncion se hizo de manera independiente. Teniendo elementos en común como: sensores, luz Neopixel, switch, arduino mini.

  2.1. **Parte del tablero,** detalle de la programación usada.

![](../images/2PF/4.png)

![](../images/2PF/5.png)

![](../images/2PF/6.png)

  2.2. **Parte de la Pata Lateral Derecha,** detalle de la programación usada.

![](../images/2PF/7.png)

  2.3. **Parte de la Pata Lateral Izquierda,** detalle de la programación usada.

![](../images/2PF/8.png)

![](../images/2PF/9.png)

![](../images/2PF/10.png)

![](../images/2PF/11.png)

**III. FABRICACIÓN**

En este item detallamos que usaremos para la Fabricación del escritorio y de la silla que es el **PROCESO DE FABRICACIÓN SUSTRACTIVA**, mediante el uso de la CNC y la máquina de CORTADORA LÁSER.

**IV. MATERIALES PARA CARPINTERÍA**

Los materiales planteados aquí están relacionados a la parte de la **Carpintería** como:

  1. **TRYPLAY FENÓLICO de 18MM** (Escritorio)

  2. **TRYPLAY FENÓLICO de 15MM** (Silla)

  3. **TRYPLAY LUPUNA de 6MM** (Escritorio)

**V. MATERIALES PARA LA PARTE ELECTRÓNICA**

Los materiales planteados aquí están relacionados a la parte **ELECTRÓNICA** como:

  1. Arduino

  2. Sensores

  3. Cable rojo, negro, azul, verde, amarillo

  4. Switch

  5. Luz Neopixel Circular

  6. Luz Neopixel Cuadrada

![](../images/2PF/12.png)

# 2. SEGUNDA PARTE

**VI. PROTOTIPADO**

Esta conformado por el prototipado, para lo cual he usado el programa de **RHINOCEROS** y el programa de **RDWORKS** que es programa que se usa para cortar la piezas que nos permite armar el prototipo diseñado y la maquina usada es: la **CORTADORA LÁSER.**

a. La primera etapa es el **PROTOTIPADO A ESCALA PEQUEÑA**, para poder armar los muebles y validar si las piezas encajan perfectamente

  1.) Corte de Escritorio, se realizo en **TRYPLAY de 6mm**, y en la cortadora láser.

  2.) Corte de Silla, se realizo en **MDF 3Mm** y en la cortadora láser.

![](../images/2PF/13.png)

  3.) Armado del prototipo Escritorio, aca hicimos varias pruebas la pieza del tablero no encajaba. Y tambien la base del escritorio no calzaba.

![](../images/2PF/14.png)

  4.) Armado del prototipo Silla.

  * Realizamos tres pruebas, en el primer prototipo las piezas no encajaban y estaban muy sueltas, de ahi volvimos hacer otro corte donde cambiamos el diseño logrando que las piezas laterales sean mas compactas asi como la parte frontal posterior e inferior.
  En el tercer corte modificamos el respaldar y el asiento. Logramos la pieza que requeriamos.

![](../images/2PF/15.png)

**VII. ARMADO DE PROTOTIPO ELECTRÓNICO**

Es el corte del **PROTOTIPADO A ESCALA 1/1** para poder prototipar la parte electrónica a escala real.

1.) Corte del Tablero.

* Se procedio a colocar los switchs y la luz Neopixel de ahi los conectamos realizamos la programación en Arduino para poder obtener las diferentes imagenes: **CARITA FELIZ, CARITA TRISTE Y CARITA NORMAL** que se veran al presionar el **SWITCH**

![](../images/2PF/20.png)

2.) Corte de pata Lateral Izquierda (Abaco)

* Se colocaron los **sensores** a la altura de pieza que contendra las bolas de madera.
* De ahi se conectaran los sensores a la **luz Neopixel** y al **arduino MINI**
* Se realizara la la programación en Arduino para poder lograr que cada vez que se muevan las bolas se prendan la luz.
* La cantidad de las bolas de madera que se moveran dependera de lo indicado en el cartel esta al costado del abaco.

![](../images/2PF/17.png)

![](../images/2PF/18.png)

![](../images/2PF/19.png)

3.) Corte de pata Lateral Derecha (Piezas Circulares)

* Se colocan los **12 sensores** se conectan a los cables y  estos se conectan a la **luz Neopixel circular** y de ahí conecta al **arduino mini.**
* Se realiza la **programación en Arduino** para poder apreciar que cada vez que se coloca una pieza se prende un color y al terminar de colocar las 12 piezas se realiza un juego de luces, como símbolo de haber logrado colocar todas las piezas.

![](../images/2PF/21.png)

![](../images/2PF/22.png)

# 3. TERCERA PARTE

**VIII. PROTOTIPADO DE JOINTS**

En este items, se ha prototipado los **joints** que usaremos en los dos tipos de mobiliarios: **ESCRITORIO** y la **SILLA.**

* Los joints fueron realizados en el programa **RHINOCEROS**
* He propuesto dos tipos de **JOINTS**: **JOINTS X/Y** y **JOINTS DENTADO**
* Los joints fueron cortados en **TRYPLAY DE 6mm**
* Fueron cortados en la **Cortadora Láser** debido a que el Laboratorio donde siempre corto estaba cerrado por medidas Sanitarias.
* Al armar el joint, pudimos confirmar que la tolerancia usada era la adecuada.

  1. Tryplay fenólico de 18mm la tolerancia es de 18.5mm

  2. Los **DOGBONES** usados es de acuerdo a la fresa de uso, en este caso es de 6.35 mm y se usa de 7mm para que la fresa pueda cortar sin esfuerzo

  3. Tryplay fenólico de 15mm la tolerancia es de 15.5mm

![](../images/2PF/23.png)

**IX. NESTING**

* Es la ubicación de la piezas que conformar los mobiliarios que hemos planteado.
* Se van a cortar en los tableros de **TRYPLAY FENÓLICO DE 18MM Y 15MM**, cuyas dimensiones son: 1.22mx2.44m

* Para cortar el escritorio usaremos 2 planchas de TRYPLAY FENÓLICO de 18mm.
* Para cortar la silla se usará 3/4 de plancha de TRYPLAY FENÓLICO de 15mm.

![](../images/2PF/24.png)

**X. COSTO DE FABRICACIÓN DE LOS MUEBLES**

En este item hemos realizado un analísis de todos los componentes ha considerar en la Fabricación de un muebles

  **1. Costo de escritorio**

  a.) Divido en 4 componentes, que conforman el **COSTO DIRECTO** de la elaboracion del escritorio.

  * **CORTE Y CARPINTERIA**, incluímos el material a usar, el corte del material en las diferentes tecnologias como: CNC y CORTADORA LÁSER, el transporte del material al laboratorio donde se va a cortar y el transporte del laboratorio al taller donde se va a realizar el acabado.

  * **MATERIALS DE ARMADO Y ACABADO**, en este item he considerado el armado del mueble y el acabado que es: 2 manos de laca selladora y 1 mano de laca Crystal.

  * **MATERIALES DE ELECTRÓNICA**, he considerado todos los materiales Electrónicos como: sensores, luces neopixeles, switch, cables de diferenes colores, arduino mini. Asi como la mano de obra que conecte este y desarrolle la programación en **ARDUINO**

  * **SERVICIO DE EMPAQUE**, se considera el tiempo de empaque del Escritorio así como el material a usar.

  b.) El **COSTO INDIRECTO**, conformado por tres componentes:

  * **GASTOS GENERALES**, considera algún imprevisto en la realización del mueble, corresponde el **4% del costo directo.**

  * **UTILIDAD**, es la ganancia que representa el **25% del costo directo.**

  * **% DE VENTAS**,corresponde el  **2% del costo directo.**

  c.) La suma del **COSTO DIRECTO** y **COSTO INDIRECTO** obtenemos:

# **COSTO TOTAL DEL ESCRITORIO asciende a S./ 2686.14 equivale a $688.75**

![](../images/2PF/25.png)

  **2. Costo de Silla.**

  a.) Divido en 4 componentes, que conforman el **COSTO DIRECTO** de la elaboracion del escritorio.

  * **CORTE Y CARPINTERIA**, incluímos el material a usar, el corte del material en las diferentes tecnologias como: ***CNC***, el transporte del material al laboratorio donde se va a cortar y el transporte del laboratorio al taller donde se va a realizar el acabado.

  * **MATERIALS DE ARMADO Y ACABADO**, en este item he considerado el armado del mueble y el acabado que es: 2 manos de laca selladora y 1 mano de laca Crystal.

  * **SERVICIO DE EMPAQUE**, se considera el tiempo de empaque de la Silla así como el material a usar.

  b.) El **COSTO INDIRECTO**, conformado por tres componentes:

  * **GASTOS GENERALES**, considera algún imprevisto en la realización del mueble, corresponde el **10% del costo directo.**

  * **UTILIDAD**, es la ganancia que representa el **50% del costo directo.**

  * **% DE VENTAS**, corresponde el  **5% del costo directo.**

  c.) La suma del **COSTO DIRECTO** y **COSTO INDIRECTO** obtenemos:

# **COSTO TOTAL DE LA SILLA asciende a S./ 977.79 equivale  a  $ 250.72**

![](../images/2PF/26.png)


## 4. ARCHIVOS EDITABLES

* Archivos de los planos del ESCRITORIO

[archivodxf](../images/2PF/proyecto escritoriofinal-CORTE1.dxf)

[archivodxf](../images/2PF/NESTING.dxf)

[archivo3dm](../images/2PF/Escritorio3d.3dm)

* Archivos de los planos del PROTOTIPADO DEL ESCRITORIO a escala pequeña

[archivodxf](../images/2PF/ESCRITORIO PROTOTIPADO.dxf)

* Archivos de los planos del PROTOTIPADO DE ELECTRÓNICA

[archivodxf](../images/2PF/CORTE ELECTRONICA.dxf)

*Archivos de los planos de la SILLA

[archivodxf](../images/2PF/SILLAFINAL.dxf)

[archivo3dm](../images/2PF/S3D.3dm)

*Archivos del JOINT DE LOS MUEBLES.

[archivodxf](../images/2PF/ESCRITORIOCNC JOIN.dxf)

*Archivos del ARDUINO de la parte electrónica.

[archivoino](../images/2PF/Abaco_sensor_infrarrojo.ino)

[archivoino](../images/2PF/Aro_Neopixel_12.ino)

[archivoino](../images/2PF/Carita_Mesa.ino)
