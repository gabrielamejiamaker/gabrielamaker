---
hide:
    - toc
---
![](../images/1PF/concepto.png)

# I. MARCO CONCEPTUAL

* La necesidad de la creación de un mueble infantil multifuncional con aspecto lúdico o interactivo.

* Planteo esto debido al gran peso que el juego tiene en el desarrollo y crecimiento infantil.

* Se destacan la importancia de convertir cualquier actividad diaria en un **juego** para los más pequeños

* El juego constituye un elemento básico en la vida de un niño, que además de divertido resulta necesario para su desarrollo.

* Pero **¿por qué es importante y qué les aporta?**

* Los niños necesitan estar activos para crecer y desarrollar sus capacidades.

* El juego es importante para el aprendizaje y desarrollo integral de los niños puesto que aprenden a conocer la vida jugando.

* Los niños necesitan hacer las cosas una y otra vez antes de aprenderlas por lo que los juegos tienen carácter formativo al hacerlos enfrentarse repetidamente a situaciones a las que deberán adaptarse y dominar.

* A través del juego los niños buscan, exploran, prueban y descubren el mundo por sí mismos, siendo un instrumento eficaz para la educación

* El juego desarrolla diferentes capacidades en el niño:

    **Físicas:** para jugar los niños se mueven, ejercitándose casi sin darse cuenta, con lo cual desarrollan su coordinación psicomotriz y la motricidad gruesa y fina; además de ser saludable para todo su cuerpo, músculos, huesos, pulmones, corazón, por el ejercicio que realizan, además de permitirles dormir bien durante la noche.

* En la conceptualizacón hemos tomado en cuenta los siguientes items necesarios para plantear el proyecto teniendo como punto central al **usuario** que son **los niños de 4 a 6 años.**

    1. **Mobiliario**, definir el tipo de mobiliario a usar, que es un **ESCRITORIO y una SILLA**

    2. **Usuario**, definio que edades usan el mobiliario, son **niños de 4 a 6 años.**

    3. **Antropometría**, definí la dimensiones del Escritorio y la Silla.

        * Escritorio de dimensiones de: 590mm de largo x 590mm de ancho por 650mm de alto

        * Silla de dimensiones de: 400mm de largo x 400mm de ancho por 450mm de alto

    4. **Aspecto Lúdico**, incluir tema del juego y aprendizaje en el mobiliario del **ESCRITORIO**

    5. **Referencias de proyectos similares** investigamos proyectos similares a nuestra propuesta son:

        * ***Mobiliario BUM PUM***

        * ***Mesa Interactiva***

        * ***Panel Interactivo***

![](../images/1PF/1.png)
