---
hide:
    - toc
---
![](../images/3PF/propuesta.png)

# III. PROPUESTA FINAL

La **propuesta final** es el desarrollo y Fabricación de un escritorio con elementos Electrónicos y una silla.

La Propuesta es validada con la **realización del prototipado** tanto a escala pequeña como a escala 1/1.

Detallara el **detalle de las piezas** asi como **vistas 3d** de las piezas.


![](../images/3PF/27.png)

3.1 **RECOMENDACIONES y CONCLUSIONES**

![](../images/3PF/28.png)

3.2 **SLIDE**

Adjuntaremos un cuadro resumen de todo los elementos que componen este proyecto desde el **CONCEPTO** hasta el **PRODUCTO FINAL**

![](../images/3PF/SLIDE-PY.png)

3.3 **VIDEO**

[archivo.mp4](https://www.youtube.com/watch?v=qs1EdIEmuzo.mp4)
