## **Bienvenido**

## Especializacion en Fabricación Digital e Innovación Abierta

El **Programa** apunta a **desarrollar profesionales** capacitados en generar propuestas innovadoras y adaptadas a las necesidades de diferentes áreas de la producción y el diseño, en nuestro país y la región, haciendo uso de metodologías de innovación abierta asistidas por tecnologías de fabricación digital.

La malla curricular se compone de:

  * **Módulos Técnicos (MT)**
  * **Módulos de Profundización (MP)**
  * **Módulos de Innovación (MI)**
  * **Un Taller Integrador presencial (TIp)**
  * **El Proyecto Final (PF).**

En la **Especialización** se exige la realización de un **Proyecto final** a realizar de forma grupal o individual, aplicado en un área específica a elección que deberá estar relacionada a un problema real que pueda solucionarse con los métodos y herramientas adquiridos en la especialización.

La **Especialización en Fabricación Digital e Innovación** te permitirá formarte con  referentes y expertos de **UTEC** y del **Fab Lab Barcelona** en el  Instituto de Arquitectura Avanzada de Cataluña  (IAAC), quienes codirigen el programa y realizan la curaduría. Fab Lab Barcelona es el primer laboratorio  de fabricación digital de la Unión Europea creado en  2007 a partir del Center for Bits and Atoms (CBA)  del Massachusetts Institute of Technology (MIT).

![](../images/contenido.jpg)

Después del recorrido de 11 meses de aprendizaje, termino este camino con muchos aprendizajes tanto a nivel académico como personal.

Ha sido una especialización que ha contribuido a mi carrera, la cual recomiendo que lo lleven.

Lo que más me ha gustado es el haber adquirido muchos conocimientos teóricos y prácticos y el conocer otras tecnologías de fabricación.

Esto ha sido la confirmación cuando elegí dedicarme a este camino no me equivoque. Es el inicio para seguir conociendo y especializando en estos temas de fabricación digital.

No quiero dejar de agredecer a mis profesores de **UTEC URUGUAY** por siempre estar dispuestos a resolver todas mis dudas.

También agradesco a los docentes de **FAB LAB BARCELONA** por compartir todas sus experiencias, conociemtos y siempre estar dispuestos a resolver las dudas que tenía.
Lo más importante agradecer a **Dios**, **la virgen María** y **a mis padres** por siempre apoyarme en todos mis sueños.
