#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        6 // Conectar al numero 6 del Arduino

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 64 // Cantidad led usados

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
int boton1 = 7;  // Ubicacion de los pulsadores en el arduino
int boton2 = 8;
int boton3 = 9;

void setup() {
  
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pinMode(boton1,INPUT);
  pinMode(boton2,INPUT);
  pinMode(boton3,INPUT);
}

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'

  int sensor1  = digitalRead(boton1);
  int sensor2  = digitalRead(boton2);
  int sensor3  = digitalRead(boton3);
  int i=0;
  if( sensor1 == LOW)
  {
    int TRISTE[]={2,3,4,5,14,9,16,18,21,23,31,27,24,32,36,39,47,45,42,40,49,54,61,60,59,58};
  for(int i=0;i<26;i++)
  {
    pixels.setPixelColor(TRISTE[58], pixels.Color(150,0,0));
    pixels.show();
  }
  }
  ////////
  if( sensor2 == LOW)
  {
    int happy[]={2,3,4,5,14,9,16,18,20,23,31,26,24,32,37,39,47,45,43,40,49,54,61,60,59,58};
  for(int i=0;i<26;i++)
  {
    pixels.setPixelColor(hap[i], pixels.Color(0,150,0));
    pixels.show();
  }
  }
  ///////
  if( sensor3 == LOW)
  {
    int gui[]={4,10,22,25,38,41,53,59};
  
  pixels.setPixelColor(13, pixels.Color(0,0,150));
  pixels.setPixelColor(18, pixels.Color(0,0,150));
  pixels.setPixelColor(46, pixels.Color(0,0,150));
  pixels.setPixelColor(45, pixels.Color(0,0,150));
  pixels.setPixelColor(49, pixels.Color(0,0,150));
  pixels.setPixelColor(50, pixels.Color(0,0,150));
  pixels.show();
  
  for(int i=0;i<8;i++)
  {
    pixels.setPixelColor(gui[i], pixels.Color(0,0,150));
    pixels.show();
    delay(50);
  }
  }

  if( sensor1 == LOW && sensor2 == LOW && sensor3 == LOW)
  {
  pixels.setPixelColor(0, pixels.Color(0,0,0));
  pixels.setPixelColor(1, pixels.Color(0,0,0));
  pixels.setPixelColor(2, pixels.Color(0,0,0));
  pixels.setPixelColor(3, pixels.Color(0,0,0));
  pixels.setPixelColor(4, pixels.Color(0,0,0));
  pixels.setPixelColor(5, pixels.Color(0,0,0));
  pixels.setPixelColor(6, pixels.Color(0,0,0));
  pixels.setPixelColor(7, pixels.Color(0,0,0));
  pixels.setPixelColor(8, pixels.Color(0,0,0));
  pixels.setPixelColor(9, pixels.Color(0,0,0));
  pixels.setPixelColor(10, pixels.Color(0,0,0));
  pixels.setPixelColor(11, pixels.Color(0,0,0));
  pixels.setPixelColor(12, pixels.Color(0,0,0));
  pixels.setPixelColor(13, pixels.Color(0,0,0));
  pixels.setPixelColor(14, pixels.Color(0,0,0));
  pixels.setPixelColor(15, pixels.Color(0,0,0));
  pixels.setPixelColor(16, pixels.Color(0,0,0));
  pixels.setPixelColor(17, pixels.Color(0,0,0));
  pixels.setPixelColor(18, pixels.Color(0,0,0));
  pixels.setPixelColor(19, pixels.Color(0,0,0));
  pixels.setPixelColor(20, pixels.Color(0,0,0));
  pixels.setPixelColor(21, pixels.Color(0,0,0));
  pixels.setPixelColor(22, pixels.Color(0,0,0));
  pixels.setPixelColor(23, pixels.Color(0,0,0));
  pixels.setPixelColor(24, pixels.Color(0,0,0));
  pixels.setPixelColor(25, pixels.Color(0,0,0));
  pixels.setPixelColor(26, pixels.Color(0,0,0));
  pixels.setPixelColor(27, pixels.Color(0,0,0));
  pixels.setPixelColor(28, pixels.Color(0,0,0));
  pixels.setPixelColor(29, pixels.Color(0,0,0));
  pixels.setPixelColor(30, pixels.Color(0,0,0));
  pixels.setPixelColor(31, pixels.Color(0,0,0));
  pixels.setPixelColor(32, pixels.Color(0,0,0));
  pixels.setPixelColor(33, pixels.Color(0,0,0));
  pixels.setPixelColor(34, pixels.Color(0,0,0));
  pixels.setPixelColor(35, pixels.Color(0,0,0));
  pixels.setPixelColor(36, pixels.Color(0,0,0));
  pixels.setPixelColor(37, pixels.Color(0,0,0));
  pixels.setPixelColor(38, pixels.Color(0,0,0));
  pixels.setPixelColor(39, pixels.Color(0,0,0));
  pixels.setPixelColor(40, pixels.Color(0,0,0));
  pixels.setPixelColor(41, pixels.Color(0,0,0));
  pixels.setPixelColor(42, pixels.Color(0,0,0));
  pixels.setPixelColor(43, pixels.Color(0,0,0));
  pixels.setPixelColor(44, pixels.Color(0,0,0));
  pixels.setPixelColor(45, pixels.Color(0,0,0));
  pixels.setPixelColor(46, pixels.Color(0,0,0));
  pixels.setPixelColor(47, pixels.Color(0,0,0));
  pixels.setPixelColor(48, pixels.Color(0,0,0));
  pixels.setPixelColor(49, pixels.Color(0,0,0));
  pixels.setPixelColor(50, pixels.Color(0,0,0));
  pixels.setPixelColor(51, pixels.Color(0,0,0));
  pixels.setPixelColor(52, pixels.Color(0,0,0));
  pixels.setPixelColor(53, pixels.Color(0,0,0));
  pixels.setPixelColor(54, pixels.Color(0,0,0));
  pixels.setPixelColor(55, pixels.Color(0,0,0));
  pixels.setPixelColor(56, pixels.Color(0,0,0));
  pixels.setPixelColor(57, pixels.Color(0,0,0));
  pixels.setPixelColor(58, pixels.Color(0,0,0));
  pixels.setPixelColor(59, pixels.Color(0,0,0));
  pixels.setPixelColor(60, pixels.Color(0,0,0));
  pixels.setPixelColor(61, pixels.Color(0,0,0));
  pixels.setPixelColor(62, pixels.Color(0,0,0));
  pixels.setPixelColor(63, pixels.Color(0,0,0));
  pixels.show(); 
  }
  
  else
  {
    pixels.setPixelColor(0, pixels.Color(0,0,0));
  pixels.setPixelColor(1, pixels.Color(0,0,0));
  pixels.setPixelColor(2, pixels.Color(0,0,0));
  pixels.setPixelColor(3, pixels.Color(0,0,0));
  pixels.setPixelColor(4, pixels.Color(0,0,0));
  pixels.setPixelColor(5, pixels.Color(0,0,0));
  pixels.setPixelColor(6, pixels.Color(0,0,0));
  pixels.setPixelColor(7, pixels.Color(0,0,0));
  pixels.setPixelColor(8, pixels.Color(0,0,0));
  pixels.setPixelColor(9, pixels.Color(0,0,0));
  pixels.setPixelColor(10, pixels.Color(0,0,0));
  pixels.setPixelColor(11, pixels.Color(0,0,0));
  pixels.setPixelColor(12, pixels.Color(0,0,0));
  pixels.setPixelColor(13, pixels.Color(0,0,0));
  pixels.setPixelColor(14, pixels.Color(0,0,0));
  pixels.setPixelColor(15, pixels.Color(0,0,0));
  pixels.setPixelColor(16, pixels.Color(0,0,0));
  pixels.setPixelColor(17, pixels.Color(0,0,0));
  pixels.setPixelColor(18, pixels.Color(0,0,0));
  pixels.setPixelColor(19, pixels.Color(0,0,0));
  pixels.setPixelColor(20, pixels.Color(0,0,0));
  pixels.setPixelColor(21, pixels.Color(0,0,0));
  pixels.setPixelColor(22, pixels.Color(0,0,0));
  pixels.setPixelColor(23, pixels.Color(0,0,0));
  pixels.setPixelColor(24, pixels.Color(0,0,0));
  pixels.setPixelColor(25, pixels.Color(0,0,0));
  pixels.setPixelColor(26, pixels.Color(0,0,0));
  pixels.setPixelColor(27, pixels.Color(0,0,0));
  pixels.setPixelColor(28, pixels.Color(0,0,0));
  pixels.setPixelColor(29, pixels.Color(0,0,0));
  pixels.setPixelColor(30, pixels.Color(0,0,0));
  pixels.setPixelColor(31, pixels.Color(0,0,0));
  pixels.setPixelColor(32, pixels.Color(0,0,0));
  pixels.setPixelColor(33, pixels.Color(0,0,0));
  pixels.setPixelColor(34, pixels.Color(0,0,0));
  pixels.setPixelColor(35, pixels.Color(0,0,0));
  pixels.setPixelColor(36, pixels.Color(0,0,0));
  pixels.setPixelColor(37, pixels.Color(0,0,0));
  pixels.setPixelColor(38, pixels.Color(0,0,0));
  pixels.setPixelColor(39, pixels.Color(0,0,0));
  pixels.setPixelColor(40, pixels.Color(0,0,0));
  pixels.setPixelColor(41, pixels.Color(0,0,0));
  pixels.setPixelColor(42, pixels.Color(0,0,0));
  pixels.setPixelColor(43, pixels.Color(0,0,0));
  pixels.setPixelColor(44, pixels.Color(0,0,0));
  pixels.setPixelColor(45, pixels.Color(0,0,0));
  pixels.setPixelColor(46, pixels.Color(0,0,0));
  pixels.setPixelColor(47, pixels.Color(0,0,0));
  pixels.setPixelColor(48, pixels.Color(0,0,0));
  pixels.setPixelColor(49, pixels.Color(0,0,0));
  pixels.setPixelColor(50, pixels.Color(0,0,0));
  pixels.setPixelColor(51, pixels.Color(0,0,0));
  pixels.setPixelColor(52, pixels.Color(0,0,0));
  pixels.setPixelColor(53, pixels.Color(0,0,0));
  pixels.setPixelColor(54, pixels.Color(0,0,0));
  pixels.setPixelColor(55, pixels.Color(0,0,0));
  pixels.setPixelColor(56, pixels.Color(0,0,0));
  pixels.setPixelColor(57, pixels.Color(0,0,0));
  pixels.setPixelColor(58, pixels.Color(0,0,0));
  pixels.setPixelColor(59, pixels.Color(0,0,0));
  pixels.setPixelColor(60, pixels.Color(0,0,0));
  pixels.setPixelColor(61, pixels.Color(0,0,0));
  pixels.setPixelColor(62, pixels.Color(0,0,0));
  pixels.setPixelColor(63, pixels.Color(0,0,0));
  pixels.show(); 
  }
}
