#include <Adafruit_NeoPixel.h>  // Libreria de Neopixel
#include <FastLED.h>  // Libreria de Juego de Luces
#ifdef __AVR__
#include <avr/power.h>
#endif
#define LED_PIN      14  // Numero de ubicacion en el Arduino
///////
#define NUM_LEDS    12  // Numero de Led Neopixel
#define BRIGHTNESS  64  // Intensidad de LUZ
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
//////////
#define UPDATES_PER_SECOND 100
const int  boton1 = 2;  // Primer sensor = Pin numero 2 del Arduino.
const int  boton2 = 3;
const int  boton3 = 4;
const int  boton4 = 5;
const int  boton5 = 6;
const int  boton6 = 7;
const int  boton7 = 8;
const int  boton8 = 9;
const int  boton9 = 10;
const int  boton10 = 11;
const int  boton11 = 12;
const int  boton12 = 13;
//////////////////
CRGBPalette16 currentPalette;
TBlendType    currentBlending; 

extern CRGBPalette16 myRedWhiteBluePalette; 
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;
///////////////
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);
void setup() 
{
  #if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  pixels.begin(); // This initializes the NeoPixel library.
  delay( 3000 ); // power-up safety delay
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );
    
    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;
  pinMode(boton1,INPUT);
  pinMode(boton2,INPUT);
  pinMode(boton3,INPUT);
  pinMode(boton4,INPUT);
  pinMode(boton5,INPUT);
  pinMode(boton6,INPUT);
  pinMode(boton7,INPUT);
  pinMode(boton8,INPUT);
  pinMode(boton9,INPUT);
  pinMode(boton10,INPUT);
  pinMode(boton11,INPUT);
  pinMode(boton12,INPUT); // Config la entrada del Pin de arduino
  pixels.clear(); // Limpia toda informacion del Neopixel
}

void loop() 
{
  int sensor1 = digitalRead(boton1);  //Lectura del sensor 
  int sensor2 = digitalRead(boton2);
  int sensor3 = digitalRead(boton3);
  int sensor4 = digitalRead(boton4);
  int sensor5 = digitalRead(boton5);
  int sensor6 = digitalRead(boton6);
  int sensor7 = digitalRead(boton7);
  int sensor8 = digitalRead(boton8);
  int sensor9 = digitalRead(boton9);
  int sensor10 = digitalRead(boton10);
  int sensor11 = digitalRead(boton11);
  int sensor12 = digitalRead(boton12);
  
  if(sensor1 == LOW)
  {                                     Red , Green , Blue
    pixels.setPixelColor(6, pixels.Color(0,50,0));
    pixels.show();
  }
  if(sensor2 == LOW)
  {
    pixels.setPixelColor(7, pixels.Color(50,0,0));
    pixels.show();
  }
  if(sensor3 == LOW)
  {
    pixels.setPixelColor(8, pixels.Color(0,0,50));
    pixels.show();
  }
  if(sensor4 == LOW)
  {
    pixels.setPixelColor(9, pixels.Color(0,50,50));
    pixels.show();
  }
  if(sensor5 == LOW)
  {
    pixels.setPixelColor(10, pixels.Color(50,50,0));
    pixels.show();
  }
  if(sensor6 == LOW)
  {
    pixels.setPixelColor(11, pixels.Color(54,54,54));
    pixels.show();
  }
  if(sensor7 == LOW)
  {
    pixels.setPixelColor(0, pixels.Color(50,20,50));
    pixels.show();
  }
  if(sensor8 == LOW)
  {
    pixels.setPixelColor(1, pixels.Color(20,0,50));
    pixels.show();
  }
  if(sensor9 == LOW)
  {
    pixels.setPixelColor(2, pixels.Color(120,50,20));
    pixels.show();
  }
  if(sensor10 == LOW)
  {
    pixels.setPixelColor(3, pixels.Color(50,20,80));
    pixels.show();
  }
  if(sensor11 == LOW)
  {
    pixels.setPixelColor(4, pixels.Color(80,10,100));
    pixels.show();
  }
  if(sensor12 == LOW)
  {
    pixels.setPixelColor(5, pixels.Color(150,0,90));
    pixels.show();
  }
  if(sensor1 == LOW & sensor2 == LOW & sensor3 == LOW && sensor4 == LOW & sensor5 == LOW & sensor6 == LOW && sensor7 == LOW & sensor8 == LOW & sensor9 == LOW && sensor10 == LOW & sensor11 == LOW & sensor12 == LOW)
  {
    ChangePalettePeriodically();
    
    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */
    
    FillLEDsFromPaletteColors( startIndex);
    
    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND); // Juego de Luces de la libreri FastLEd
  }
  else
  {
    pixels.setPixelColor(0, pixels.Color(0,0,0));
    pixels.setPixelColor(1, pixels.Color(0,0,0));
    pixels.setPixelColor(2, pixels.Color(0,0,0));
    pixels.setPixelColor(3, pixels.Color(0,0,0));
    pixels.setPixelColor(4, pixels.Color(0,0,0));
    pixels.setPixelColor(5, pixels.Color(0,0,0));
    pixels.setPixelColor(6, pixels.Color(0,0,0));
    pixels.setPixelColor(7, pixels.Color(0,0,0));
    pixels.setPixelColor(8, pixels.Color(0,0,0));
    pixels.setPixelColor(9, pixels.Color(0,0,0));
    pixels.setPixelColor(10, pixels.Color(0,0,0));
    pixels.setPixelColor(11, pixels.Color(0,0,0));
    pixels.show(); // Muestra el color en el neopixel
  }
}
////////////////
void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
    uint8_t brightness = 255;
    
    for( int i = 0; i < NUM_LEDS; ++i) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
}


// There are several different palettes of colors demonstrated here.
//
// FastLED provides several 'preset' palettes: RainbowColors_p, RainbowStripeColors_p,
// OceanColors_p, CloudColors_p, LavaColors_p, ForestColors_p, and PartyColors_p.
//
// Additionally, you can manually define your own color palettes, or you can write
// code that creates color palettes on the fly.  All are shown here.

void ChangePalettePeriodically()
{
    uint8_t secondHand = (millis() / 1000) % 60;
    static uint8_t lastSecond = 99;
    
    if( lastSecond != secondHand) {
        lastSecond = secondHand;
        if( secondHand ==  0)  { currentPalette = RainbowColors_p;         currentBlending = LINEARBLEND; }
        if( secondHand == 10)  { currentPalette = RainbowStripeColors_p;   currentBlending = NOBLEND;  }
        if( secondHand == 15)  { currentPalette = RainbowStripeColors_p;   currentBlending = LINEARBLEND; }
        if( secondHand == 20)  { SetupPurpleAndGreenPalette();             currentBlending = LINEARBLEND; }
        if( secondHand == 25)  { SetupTotallyRandomPalette();              currentBlending = LINEARBLEND; }
        if( secondHand == 30)  { SetupBlackAndWhiteStripedPalette();       currentBlending = NOBLEND; }
        if( secondHand == 35)  { SetupBlackAndWhiteStripedPalette();       currentBlending = LINEARBLEND; }
        if( secondHand == 40)  { currentPalette = CloudColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 45)  { currentPalette = PartyColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 50)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = NOBLEND;  }
        if( secondHand == 55)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = LINEARBLEND; }
    }
}

// This function fills the palette with totally random colors.
void SetupTotallyRandomPalette()
{
    for( int i = 0; i < 16; ++i) {
        currentPalette[i] = CHSV( random8(), 255, random8());
    }
}

// This function sets up a palette of black and white stripes,
// using code.  Since the palette is effectively an array of
// sixteen CRGB colors, the various fill_* functions can be used
// to set them up.
void SetupBlackAndWhiteStripedPalette()
{
    // 'black out' all 16 palette entries...
    fill_solid( currentPalette, 16, CRGB::Black);
    // and set every fourth one to white.
    currentPalette[0] = CRGB::White;
    currentPalette[4] = CRGB::White;
    currentPalette[8] = CRGB::White;
    currentPalette[12] = CRGB::White;
    
}

// This function sets up a palette of purple and green stripes.
void SetupPurpleAndGreenPalette()
{
    CRGB purple = CHSV( HUE_PURPLE, 255, 255);
    CRGB green  = CHSV( HUE_GREEN, 255, 255);
    CRGB black  = CRGB::Black;
    
    currentPalette = CRGBPalette16(
                                   green,  green,  black,  black,
                                   purple, purple, black,  black,
                                   green,  green,  black,  black,
                                   purple, purple, black,  black );
}


// This example shows how to set up a static color palette
// which is stored in PROGMEM (flash), which is almost always more
// plentiful than RAM.  A static PROGMEM palette like this
// takes up 64 bytes of flash.
const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
    CRGB::Red,
    CRGB::Gray, // 'white' is too bright compared to red and blue
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Red,
    CRGB::Gray,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Blue,
    CRGB::Black,
    CRGB::Black
};
