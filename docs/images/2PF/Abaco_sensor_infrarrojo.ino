const int sensor1 = 7;
const int sensor2 = 8;
const int sensor3 = 9;
const int sensor4 = 10;
const int sensor5 = 11;
int led = 13;
int buttonState1 = 0;
int buttonState2 = 0;
int buttonState3 = 0;
int buttonState4 = 0;
int buttonState5 = 0;


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        6 // On Trinket or Gemma, suggest changing this to 1

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 1 // Popular NeoPixel ring size
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  pinMode(sensor1,INPUT);
  pinMode(sensor2,INPUT);
  pinMode(sensor3,INPUT);
  pinMode(sensor4,INPUT);
  pinMode(sensor5,INPUT);
  pinMode(led,OUTPUT);
  Serial.begin(9600);
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  pixels.begin();
}

void loop() 
{
  pixels.clear();
  buttonState1 = digitalRead(sensor1);
  buttonState2 = digitalRead(sensor2);
  buttonState3 = digitalRead(sensor3);
  buttonState4 = digitalRead(sensor4);
  buttonState5 = digitalRead(sensor5);

    if( buttonState1 == LOW )
    {
      pixels.setPixelColor(0,pixels.Color(120,0,0));  // R G B
      pixels.show();
    }
    if( buttonState2 == LOW )
    {
      pixels.setPixelColor(0,pixels.Color(0,120,0));
      pixels.show();
    }
    if( buttonState3 == LOW )
    {
      pixels.setPixelColor(0,pixels.Color(120,120,0));
      pixels.show();
    }
    if( buttonState4 == LOW )
    {
      pixels.setPixelColor(0,pixels.Color(120,0,120));
      pixels.show();
    }
    if( buttonState5 == LOW )
    {
      pixels.setPixelColor(0,pixels.Color(0,120,120));
      pixels.show();
    }
    if( buttonState1 == LOW && buttonState2 == LOW && buttonState3 == LOW && buttonState4 == LOW && buttonState5 == LOW )
    {
      pixels.setPixelColor(0,pixels.Color(120,120,120));
      pixels.show();
    }
    else
    {
      pixels.setPixelColor(0,pixels.Color(0,0,0));
      pixels.show();
    }
}
