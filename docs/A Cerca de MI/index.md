# Sobre mi

![](../images/A cerca de mi/1.jpg)

Yo soy ***Gabriela Mejia Ramírez***, **arquitecta** de profesión de la **Universidad Ricardo Palma,** maker de vocación

* Me gusta explorar el area de diseño desde el ámbito urbano pasando por diseño interior y especializandome en diseño de mobiliario.

* Nací en la ciudad de Cajamarca, actualmente radico en Lima (Perú). Mi **formación** esta relacionada al ordenamiento territorial, desarrollo económico local, design manager.
He realizado cursos relacionados a la fabricacion digital: Rhinoceros, grasshopper, carpintería digital.

* Actualmente formo parte de la comunidad **WOMAN DESIGNER**, es un grupo de conformado por mujeres relacionadas a la tecnología, es un grupo colaborativo nos reunimos para crear y aportar con inicitivas para soluciones locales.

* En calidad de **instructora** participo en el programa iLearn de la empresa iFurniture en area de administración y contabilidad.

* Coloboro en el **Fab Lab iFurniture** en el área de fabricación de muebles.

* Actualmente laboro en el area comercial y de diseño iFurniture Store.

* Los conocimientos adquiridos me permitió crear mi emprendimiento **"DIVINA GRAZIA"** donde elaboramos piezas y mobiliario con contenido católico.
* Desarrollo proyectos de arquitectura y remodelación de ambientes.



 **[my website](https://www.utec.edu.pe/)**
