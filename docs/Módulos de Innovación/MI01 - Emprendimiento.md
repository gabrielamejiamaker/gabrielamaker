---
hide:
    - toc
---

# MI 01

![](../images/MI/caratula1.png){heigth="100"}

## I. BASE CONCEPTUAL

  a.) **Business Model Canvas**
  Es una manera visual de planificar tu negocio
  Es una herramienta que puede utilizarse en lo cotidiano de la empresa y de manera más informal que un plan de negocios
  El mapa visual se divide en nueve partes, cada una de ellas describe uno de los elementos que conforman una empresa.

  1. Segmentos de clientes

  2. Propuesta de valor

  3. Canales

  4. Relación con el Cliente

  5. Fuentes de Ingresos

  6. Recursos Claves

  7. Actividades Claves

  8. Socios Claves

  9. Estructura de Costos

![](../images/MI/Canvas.png)

  b.) **Roles de Equipo:** en esta imagen están los roles de cada

![](../images/MI/roles.jpg){height="1200"}

  c.) **Perfiles de Equipo**

![](../images/MI/perfiles equipo.JPG){width="600"}

  d.) **Human Center Design:** Es el diseño centrado en el **ser humano**
  Es un enfoque para la resolución de problemas comúnmente utilizado en marcos de diseño y gestión que desarrolla soluciones a los problemas al involucrar la perspectiva humana en todos los pasos del proceso de resolución de problemas.

![](../images/MI/human.JPG){width="600"}

  **Principios:**

  1. El diseño se basa en la comprensión clara de los usuarios, el uso y el entorno
  2. Los usuarios se involucran en todo el proceso de diseño y de desarrollo
  3. El diseño se desarrolla y optimiza mediante una evaluación centrada en el usuario
  4. Se trata de un proceso iterativo
  5. El diseño tiene en cuenta toda la experiencia del usuario
  6. En el equipo de diseño hay personas con habilidades y perspectivas multidisciplinarias

  e.) **Value Proposition Canvas**

![](../images/MI/value.JPG)

  f.) **Mística relacionada al Marketing**

  1. Es intrínsecamente interesante

  2. Genera un relato alrededor de un proyecto

  3. Vincula al proyecto con el mundo a su alrededor

  4. Mantiene al proyecto relevante

  5. Se enfoca en el POR QUÉ y atrae a consumidores que comparten los mismos valores

  6. Fomenta la lealtad

  7. Motiva a quienes la comparten: consumidores, empleados y fundadores.

  g.) **Enfoques Interconectados.**

  ![](../images/MI/enfoques.JPG){width="800"}

  h.) **Propósito:** se debería manifestar en todo lo que una marca hace: desde el desarrollo del producto en sí, a la experiencia del consumidor, a cómo debería realizar sus acciones de marketing. va el gráfico de círculos

  Tenemos dos conjuntos de acciones:

  a. Performance Marketing

  ![](../images/MI/perfomance.JPG){width="700"}

  b. Brand Marketing

![](../images/MI/brand.JPG){width="700"}

  i.) **Design Sprint:** es una metodología que permite prototipar y validar ideas con usuarios finales de manera rápida, con el fin de definir el roadmap de un producto en 5 fases.

  1. **Entender**

  2. **Bocetar**

  3. **Decidir**

  4. **Prototipar**

  5. **Validar**

![](../images/MI/DesignSprint fases.png)

  j.) **Key Performance Indicators (KPI):** Es **Medidor de Desempeño,** hace referencia a una serie de métricas que se utilizan para sintetizar la información sobre la eficacia y productividad de las acciones que se lleven a cabo en un negocio con el fin de poder tomar decisiones y determinar aquellas que han sido más efectivas a la hora de cumplir con los objetivos marcados en un proceso o proyecto concreto.

  Son utilizados de una forma muy habitual en el marketing online.
  Los KPI son utilizados por diversas ventajas:

  1. Permiten obtener información valiosa y útil.

  2. Medir determinadas variables y resultados a partir de dicha información.

  3. Analizar la información y efectos de unas determinadas estrategias (así como las tareas que se utilizaron para llevar a cabo las mismas).

  4. Comparar la información y determinar las estrategias y tareas efectivas.

  5. Tomar las decisiones oportunas.

      ![](../images/MI/kpi.png){width="400"}

  k.) **Diagrama de Gantt:** es una herramienta de gestión de proyectos en la que se recoge la planificación de un proyecto. Normalmente tiene dos secciones: en la parte izquierda se incluye una lista de tareas y, en la derecha, un cronograma con barras que representan el trabajo. Los diagramas de Gantt también pueden incluir las fechas de inicio y de finalización de las tareas, los hitos, las dependencias entre tareas y las personas asignadas

  Tiene tres principales fines:

  1. **Crear y gestionar un proyecto completo**

  2. **Determinar la logística y las dependencias de las tareas**

  3. **Supervisar el progreso de un proyecto**

![](../images/MI/gant.JPG){height="4000"}

## II.- HERRAMIENTAS UTILES

  a.) En el gráfico detallare las herramientas que según lo estudiado son importantes para el desarrollo de mi proyecto.
  Estas herramientas me permiten programar los pasos del proyecto como el diagrama de gantt y el bussines canvas en donde planificamos los diferentes segmentos que necesitamos.
  Aprendí la importancia del marketing, asi como el propósito y la mística que debemos de considerar al iniciar cualquier proyecto.

![](../images/MI/herramientas.jpg){width="600"}
