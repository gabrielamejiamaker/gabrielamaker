---
hide:
    - toc
---

# MI 02

![](../images/MI-02/grass.jpg)

## I. BASE CONCEPTUAL

1. **Diseño Computacional:** Es la aplicación de estrategias computacionales para el proceso de diseño.
  Es uno de los grandes protagonistas de la transformación digital
  Tiene como objetivo mejorar ese proceso mediante la codificación de las decisiones de diseño utilizando un lenguaje de programación.
  El objetivo no es necesariamente documentar el resultado final, sino más bien los pasos necesarios para crear ese resultado.
  Integra profesionales de diferentes disciplinas en un colectivo creativo donde la ciencia, la técnica y la creatividad se unen en torno a los procesos de concepción y desarrollo, en un flujo de trabajo continuo que va desde la conceptualización hasta la fabricación. Este nuevo entorno creativo promueve ideas que desafían la viabilidad y la capacidad técnica de cara a los desafíos de la sostenibilidad.

  El pensamiento computacional, en el ámbito del diseño, es en sí mismo una oportunidad para avanzar en la solución de problemas complejos. Se apoya en la gestión de datos para generar nuevas oportunidades de negocios

2. Herramientas computacionales de diseño
  Hay una serie de herramientas de diseño computacional en el mercado. La mayoría de estas herramientas trabajan sobre otras plataformas de software, como Microstation, Rhino o Revit.

  Las cinco herramientas más populares de diseño informático.

  a.) **Generative Components**, es el padre de las herramientas de diseño computacional. Fue introducido por primera vez en 2003 y lanzado al mercado en 2007. Generative Components funciona con el software Microstation aunque una versión independiente está disponible.

  b.) **Grasshopper**, es sin duda la herramienta más popular de diseño computacional. Grasshopper es una herramienta de modelado algorítmico para Rhino, el software de modelado 3D por Robert McNeel & Associates. Grasshopper ha existido desde hace más de ocho años y tiene fanáticos seguidores. Es un producto muy maduro con una amplia biblioteca de nodos.

  c.) **Dynamo**, es una herramienta de programación visual de Autodesk. Está disponible en una versión gratuita que enlaza directamente con Revit, así como un pago independiente. Dynamo está creciendo en popularidad y tiene una comunidad activa que está desarrollando nodos para soportar una amplia gama de usos.

  d.) **Marionette**, es un nuevo producto de Vectorworks. Está construido directamente en Vectorworks 2016. Marionette es una multiplataforma por lo que funciona tanto en Mac como Windows.

  e.) **Flux**, es un producto de Google[x], el laboratorio de investigación de Google. Flux es único, ya que funciona a través de plataformas mediante una interfaz basada en la web. Al utilizar Flux se pueden compartir datos entre aplicaciones.

  3. Beneficios del Diseño Computacional

      a.) **Explorar múltiples opciones de diseño**

      b.) **Ensuciarse las manos y acceder a tus datos**

      c.) **Automatizar tareas repetitivas**

      d.) **Prueba lo que tu diseño realmente está haciendo**

      e.) **Piense algorítmicamente**

  4. **Tipos de diseño**

  * Tenemos diferentes tipos de diseño:

    ![](../images/MI-02/diseño1.JPG)

    ![](../images/MI-02/diseño2.JPG)

  5. **Grasshopper:** es un plugin de Rhino, usado para diseño paramétrico
  Sus componentes tienen entradas y salidas, las salidas se conectan a las entradas de los componentes subsecuentes. Es utilizado principalmente para programar algoritmos generativos

  La principal interfaz para el diseño de algoritmos en Grasshopper es el editor basado en nodos.
  La información puede ser también definida de manera local como una constante, puede ser también importada desde un documento existente de Rhino. La información es almacenada en parámetros, los mismos que pueden estar conectados o no a otros

## II. DESAFIO##

  2.1 Elegir uno de los ejercicios en clase, editarlo y documentar

  De los ejercicios realizados en clase, elegí fue el de las columnas, me pareció un ejercicio que me permitió ver como se puede parametrizar el alto y ancho de estas.

![](../images/MI-02/ejercicio8.jpg)

  Adjunto otros ejercicios realizados en clase: ejercicios sobre listas, división de curvas.

  ![](../images/MI-02/ejercicio1.JPG)

  ![](../images/MI-02/ejercicio2.JPG)

2.2 Elegir un ejercicio relacionado a tu proyecto final para desarrollar con grasshopper y documentar

  * El objeto que voy a realizar es una pieza que formara parte de mi mueble.
  * Realice un boceto de como sería la pieza
  * Dimensione la pieza para poder dibujarla y parametrizarla
  * Revisión de comandos de grasshopper.
  * Antes de empezar a colocar las baterias, vi opciones de como se podía realizar.

    1. Sería mediante el uso de líneas
    2. Realizar un rectángulo.

  * Elegí realizar un rectangulos grande y de ahi realizar varios rectángulos son parte huecas donde iran las piezas que se cortaron en corte láser.

  * Unir toda la pieza con boundary
  * Procederá a extruir la pieza a 18mm
  * Se realizara la diferencia boleana y obtendremos la pieza final.

![](../images/MI-02/ejemplo1.JPG)

![](../images/MI-02/ejemplo2.JPG)

![](../images/MI-02/ejemplo3.JPG)

## III. TIPS.

  1. Grabar el archivo de grasshopper antes de salir sino se pierde el trabajo.
  2. De ahi grabar el archivo de Rhinoceros
  3. Antes de empezar a trabajar en la laptop bocetear en tu cuaderno como sería el esquema
  4. Revisión de los comandos

## IV. ARCHIVOS EDITABLES

[archivo3dm](../images/MI-02/circulo1.3dm)

[archivogh](../images/MI-02/circulo.gh)

[archivo3dm](../images/MI-02/columnas.3dm)

[archivogh](../images/MI-02/columnas.gh)

[archivo3dm](../images/MI-02/curvaconcolumnas.3dm)

[archivogh](../images/MI-02/curvacon columnas.gh)

[archivo3dm](../images/MI-02/tablero1.3dm)

[archivogh](../images/MI-02/tablero.gh)
