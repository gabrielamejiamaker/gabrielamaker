---
hide:
    - toc
---

# MI 03

![](../images/MI-03/caratula.png)

## I. BASE CONCEPTUAL

a.) **Innovación Abierta**

b.) **Proceso de Innovación Abierta**

![](../images/MI-03/Diapositiva1.JPG)

c.) **Puntos claves de la Innovación, esta conformado por 10 puntos.**


![](../images/MI-03/Diapositiva2.JPG)


d.) **Reglas de la Innovación, conformado por 4 items**


![](../images/MI-03/Diapositiva3.JPG)


e.) **Modelos de la Innovación,** tenemos 5 modelos.


![](../images/MI-03/Diapositiva4.JPG)

En estas imágenes describiremos cada modelo de innovación.

![](../images/MI-03/Diapositiva5.JPG)
![](../images/MI-03/Diapositiva6.JPG)

![](../images/MI-03/Diapositiva7.JPG)

f.) ***Conclusiones***

![](../images/MI-03/Diapositiva8.JPG)

g.) **Conceptos relacionado Open Source y Software Libre.**

![](../images/MI-03/Diapositiva9.JPG)

h.) **Conceptos de Código Abierto y Open Harddware.**

![](../images/MI-03/Diapositiva10.JPG)

![](../images/MI-03/Diapositiva11.JPG)

i.) **Que es Licencia y GPL**

![](../images/MI-03/Diapositiva12.JPG)

j.) **Tipos de plataformas**

![](../images/MI-03/Diapositiva13.JPG)

k.) ***Conclusiones***

![](../images/MI-03/Diapositiva14.JPG)

## II. DESAFIO##

a.) Ingresar al enlace de **MIRO** escoja un tablero, coloque su nombre al costado del mismo    

![](../images/MI-03/Diapositiva16.jpg){width="650"}

Colocar 3 ideas (mínimo) 5 ideas (máximo)  de cómo mejorarían nuestra red de laboratorios de innovación Abierto    

b.) Elegir una **fuente de Código Abierto** (puede ser Open Source Hardware o Software                                  Justificar la elección de la fuente de a cuerdo a su posible utilización en el**PROYECTOFINAL**     

![](../images/MI-03/Diapositiva15.JPG)
