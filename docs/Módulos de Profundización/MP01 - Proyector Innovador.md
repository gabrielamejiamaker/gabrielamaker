---
hide:
    - toc
---

# MP 01

![](../images/MP01/fabricacion1.jpg)

## INTRODUCCION A LA FABRICACION DIGITAL E INNOVACION/ PROYECTO INNOVADOR

## I.BASE CONCEPTUAL

a.) **Fab labs:** son ***espacios*** donde se produce la combinación de procesos de fabricación digital
Permiten el acceso a los ciudadanos a estas tecnologías, para poder dar respuestas a soluciones de sus localidades.

Este movimiento de los Fab Labs, nació hace 14 años y actualmente hay más de 2000 Fab Labs en el mundo.
La finalidad de los Fab Labs es democratizar el acceso a la tecnología y al acceso a las herramientas de producción para poder impactar a la población.

Estamos sufriendo un cambio en la producción de las cosas, lo que ahora viaja es la información de cómo se fabrica el producto mas no el producto
Se está pasando de un modelo centralizado de fabricación a un modelo fabricación local distribuida

  ![](../images/MP01/fablab.JPG)

b.) **Fabricación Digital:** Es una herramienta que permite trabajar entre los mundos de los BITS y el mundo de los ATOM.
Entre lo digital y lo físico, entre los prototipos y las ideas

Este movimiento empezó en 1950 donde nace la Primera CNC, de ahí se da un salto a 1980 con la primera PC.
En 1984 surge algo importante es el proceso de impresión 3d que es un proceso aditivo que ha ido evolucionando.
En 2005, Se creó la primera impresora 3d, y se liberó la patente, se creó un proyecto Open Source

![](../images/MP01/fabricacion.JPG)

c.) **Procesos de Fabricación Aditivos:** es un nuevo **concepto de producción** a través del cual el material (plástico o metal) es depositado capa a capa de manera controlada allí donde es necesario
Mediante esta técnica, que conocemos como impresión 3D, se pueden producir formas geométricas personalizadas en función de las necesidades de cada sector.

![](../images/MP01/proceso.JPG)

d.) **Proceso de Fabricación Sustractivo:** es cualquier proceso en el que las piezas se **producen al eliminar el material** de un bloque sólido, barras de plástico, metal u otros materiales eliminados mediante corte, perforación y esmerilado para producir la forma deseada.
Se llevan a cabo con un control numérico computerizado (CNC).

## II.DESAFIO
Seleccionar 3 o mas proyectos que se relacionen a tu idea de proyecto final

a.) Proyecto1: **Mobiliario Interactivo BUM-PA,** elegí este proyecto, utiliza CAD en el diseño asi como CAM, en la fabricación. El uso de maderas propias del lugar donde se realizo el proyecto, así como la inclusión de la música como medio de aprendizaje

<a href=https://www.madera21.cl/blog/project-view/mobiliario-interactivo-para-ninos/" target="_blank">Enlace a otra web</a>

b.) Proyecto2: **Mobiliario Infantil,** es un emprendimiento que realiza mobiliarios para niños que incluye el juego como medio de aprendizaje. Esto se aprecia en los ejemplos citados como:
Mesa de juego, Paneles Interactivos y Playscape.

<a href=https://www.thelibrarystore.com/" target="_blank">Enlace a otra web</a>


c.) Proyecto3: **Mobiliario Interactivo,** es un mueble que incluye una pantalla táctil para la diversión de los niños que promueve el aprendizaje e interactividad entre ellos. Es interesante el uso de tecnología donde el mueble no solo tiene la función de almacenar o guardar sino incluye elementos adicionales como electrónica, inteligencia artificial, entre otros.

<a href=https://kylii-kids.com/es/product/mesa-tactil-infantil-multijugador.com/" target="_blank">Enlace a otra web</a>

## III. REFLEXIÓN

a.) Al investigar estos tres proyectos, cada uno tenía elementos que deseo incorporar en mi proyecto Final

**1.) Uso de diseño CAD**

**2.) Uso de CAM, especialmente en la fabricación de mi producto.**

**3.) Elementos Interactivos promuevan el juego y aprendizaje del niño**

**4.) Elementos relacionados a la electrónica o a la inteligencia artificial**

**5.) Uso de materiales ecoamigables con el medio ambiente.**

![](../images/MP01/Trabajoidea.JPG){width="1400"}
