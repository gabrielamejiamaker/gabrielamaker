---
hide:
    - toc
---

# MP 02

![](../images/MP02/prototipado.jpg)


## I.BASE CONCEPTUAL

1.) Primer contenido: **ATLAS DE FUTUROS EMERGENTES**

  * **Idea de PROYECTO,** es aquello que nos acompaña desde el principio del proceso creativo.

  * **Señales Débiles,** un asunto que esta emergiendo de un nuevo fenómeno

  * **Conceptos detonantes,** despiertan una nueva posibilidad en tu espacio de **DISEÑO**

  * **Areas de Oportunidad,** representan herramientas, infraestructura o tecnologías que asisten a un futuro emergente o un proceso de prototipado

2.) Segundo Contenido: **COMO COMUNICAR TU PROYECTO**

a.) Encuentra tu **TRIBU:** Identificar tu audiencia, tu target, a quien le interesa tu proyecto, buscar una manera de comunicarte eficazmente con ellos

**Considerar:**

* Edad
* Género
* Nivel Educativo
* Cultura
* Necesidades

b.) **DESCUBRES** lo que haces:

* Realizar como un tweet
* Describe lo que haces hoy
* Realiza el test de la abuela
* Escribe 20 versiones de lo que realmente describe lo que haces

c.) Hazlo **VISUAL**

Hacer visual una marca a tener presente a la **AUDIENCIA,** evitar las **TENDENCIAS** de Diseño, no **COPIAR** a las marcas del sector, ser **HONESTO** con la esencia de tu Idea

Mediante la creacion de un kit de Identidad:

* **Brief,** permite considerar aspectos: especialidad, audiencia, espiritu, entre otros.

* **Logo,** trasmitir un concepto único, graficamente muy simple, literal

* **Paleta de Colores** representado 60% por un color primario, 30% por un color secundario y 10% un color que proporcione acento

* **MoodBoard,** representar como seve  y se siente la marca.

* Elección de **tu tipografía**

d.) Crea tu **COMUNIDAD**
Es necesario tomar en cuenta:

* Abre tu proyecto al mundo
* Muestra el progreso de tu trabajo
* Utiliza principios de código abierto y herramientas de google
* Sé HONESTO
* Comparte proyectos que sigas
* Natural

e.) Haz un **PLAN** y **PRUEBA**

* Crea un calendario de contendio para tus publicaciones
* Probar todos los canales

3. Tercer Contenido: **ESPACIO DE DISEÑO MULTIESCALAR**

* Proceso Fluido Interactivo
* Dimensiones de Indagación
* Disposición no lineal de Posibilidades
* Multidisciplinariedad
* Comunicarse con colaboradores y mentores
* Permitir la participación
* Recoletar feedback
* Compartir recursos para formar posibles intervenciones colectivas  

## II. PROTOTIPADO DE MI IDEA DE PROYECTO

Dentro de los contenidos del curso, rescate estos elementos que me ayudan a mi identidad de Marca

a.) **Encuentra tu **TRIBU:**

![](../images/MP02/audiencia1.jpg)

![](../images/MP02/audiencia2.jpg)

![](../images/MP02/usuario.jpg)

b.) **Hazlo VISUAL**

![](../images/MP02/brief1.JPG)

![](../images/MP02/brief2.JPG)

![](../images/MP02/brief3.JPG)

c.) **Realización MOOMBOARD**

![](../images/MP02/moon.jpg)

d.) **Realizacion del KIT de identidad VISUAL**

![](../images/MP02/logo.jpg)
