---
hide:
    - toc
---

# MP 03

![](../images/MP03/caratula.png)


## I.BASE CONCEPTUAL

a.).**Principios de la Economía Circular**

![](../images/MP03/Diapositiva1.PNG)

b.) **Diseñador de Materiales**

![](../images/MP03/Diapositiva2.PNG)

c.) **Casos de Estudio relacionado al uso de Bio Materiales.**

![](../images/MP03/Diapositiva3.PNG)

![](../images/MP03/Diapositiva4.PNG)

d.) **Rueda Estrategia de Ecodiseño.**

![](../images/MP03/Diapositiva5.PNG)

e.) **Componentes para fabricar biomateriales**

![](../images/MP03/Diapositiva6.PNG)

f.) **Consideraciones para secado, seguridad y tipo de Moldes.**

![](../images/MP03/Diapositiva7.PNG)

## II. DESAFIO

a.) Realizar los ejercicios propuestos por Prof. Laura

![](../images/MP03/1.jpg)

a.1) Ejercicio de **AGAR**

![](../images/MP03/2.jpg)

![](../images/MP03/3.jpg)

a.2) Ejercicio de **GELATINA**

![](../images/MP03/5.jpg)

![](../images/MP03/6.jpg)

a.3) Ejercicio de **RESINA**

![](../images/MP03/4.jpg)
