---
hide:
    - toc
---

![](../images/TI/Caratula-TIP.png)

# I. DESAFIO

La práctica para realizar en equipo fue desmontar y experimentar con el funcionamiento de una extrusora de pasta.

# II. DESARROLLO

  a. Contamos con una impresora Anet A8 que debíamos adaptarle un mecanismo extrusor de pasta que fue impreso anteriormente por el equipo de **UTEC**.Para lo cual procedimos a desarmarla y luego armarla para poder colocar el extrusor para la arcilla

![](../images/TI/1.png)

![](../images/TI/2.png)

  b. Experimentar con el software de slice para alcanzar los mejores resultados

  c. La mayor dificultad fue determinar y obtener la mejor consistencia de la arcilla que se va utilizar para imprimir.

  d. La presencia de grumos y la densidad no uniforme hacía difícil obtener un funcionamiento adecuado del sistema.

![](../images/TI/3.png)

  e. Realizamos el soldado de la placa que elaboramos en el **Módulo Técnico 08**. Aparentemente parecía super fácil pero no fue así.

    * No colocar mas estaño de lo necesario.

    * Al soldar el estaño no se salga del ámbito de la pieza.

    * Colocar de forma ordenada las piezas e ir soldadando de una a una

# III. TRABAJO GRUPAL

a. Adjuntamos el trabajo grupal, de los tres días de trabajo en el Campus UTEC.

[archivopptx](../images/TI/MontajeextrusordepastaGabyFranciscoRobert.pptx)

# IV. APORTES

* Sobre el taller presencial (3 días) para mí como extranjera lo que aportó fue la oportunidad de conocer a a mis compañeros y docentes  y generar un vinculo de integración entre todos nosotros.

* Me quede con las ganas de poder experimentar en las otras tipos de tecnologías que llevamos de manera teórica como: cortar en una CNC y en una CORTADORA LÁSER.

* Agradesco la hospitalidad recibida durante mi estadia en Uruguay. Espero regresar pronto....

# V. VIDEOS

[archivo.mp4](https://www.youtube.com/watch?v=DM82kgXeZsI.mp4)

[archivo.mp4](https://www.youtube.com/watch?v=k8RZgZ-NZM0.mp4)
