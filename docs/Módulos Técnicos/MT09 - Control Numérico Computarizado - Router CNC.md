---
hide:
    - toc
---

# MT 09

![](../images/MT09/Caratula.png)

## I. BASE CONCEPTUAL

a.) **Definición de Control Numérico por Computador**

![](../images/MT09/Diapositiva1.JPG)

b.) **Partes de una CNC**

![](../images/MT09/Diapositiva2.JPG)

c.) **Tipos de CNC- routern y Spindle**

![](../images/MT09/Diapositiva3.JPG)

![](../images/MT09/Diapositiva4.JPG)

d.) **Tipos de Lubricación**

![](../images/MT09/Diapositiva5.JPG)

e.) **Consideraciones de Diseño para la Fabricación en CNC**

![](../images/MT09/Diapositiva6.JPG)

![](../images/MT09/Diapositiva7.JPG)

![](../images/MT09/Diapositiva8.JPG)

![](../images/MT09/Diapositiva9.JPG)

![](../images/MT09/Diapositiva10.JPG)

f.) **Tipos de Fresa**

![](../images/MT09/Diapositiva11.JPG)

g.) **Drills vs Endmill**

![](../images/MT09/Diapositiva12.JPG)

![](../images/MT09/Diapositiva13.JPG)

h.) **Climb  vs Convecional**

![](../images/MT09/Diapositiva14.JPG)

i.) **Materiales que corta una CNC**

![](../images/MT09/Diapositiva15.JPG)

j.) **Fijacion y Cama vacia**

![](../images/MT09/Diapositiva16.JPG)

k.) **Ranura T y cinta Doble caratula**

![](../images/MT09/Diapositiva17.JPG)

l.) **Consideraciones de seguridad**

![](../images/MT09/Diapositiva18.JPG)

m.) **Recomendaciones**

![](../images/MT09/Diapositiva19.JPG)

n.) **Aplicaciones con el uso de la CNC**

![](../images/MT09/Diapositiva20.JPG)

o.) Uso del programa **EASEL CAM** para cortar en la CNC

![](../images/MT09/Diapositiva21.JPG)

![](../images/MT09/Diapositiva22.JPG)

![](../images/MT09/Diapositiva23.JPG)

![](../images/MT09/Diapositiva24.JPG)

![](../images/MT09/Diapositiva25.JPG)

## II. DESAFIO

  a.) Modelar piezas entren en un espacio de 1mx1m1m, que utilice uniones de madera sin tornillos, sin cola solo uniones secas.

  Para realizar este ejercicio, hemos diseñado una silla que sera parte del **PROYECTO FINAL**

## III. DESARROLLO DEL DESAFIO

Para realizar este desafio, decidí realizar la silla que será parte de mi proyecto final.

* Primer lugar realice un boceto a mano alzada. Esta inspirado en la primera letra de mi país es la **P** de PERÚ.

* Conforme fui avanzando en el diseño, y realice un prototipo me di cuenta que las piezas en esa forma no le proporcionaban estabilidad a la silla asi que decidi extender la parte abierta de las piezas y asi darle estabilidad a la silla.

![](../images/MT09/2.png)

* El archivo lo realize en el programa **RHINOCEROS**.

* Durante el proceso de diseño definí el tipo de material a usar que es: **TRYPLAY FENÓLICO DE 15MM**.

* Definí el tipo de encastre, use dos tipos de encastres:

    a.) **Encastre X/Y**, cuya tolerancia tiene 15.5MM.

    b.) **Encastre Dentado,** cuya tolerancia es de 15.5mm

* Revisando el archivo, se podria mejorar dandole una boleada a las esquinas que sobresale.

  ![](../images/MT09/1.png)

* Se realizo el nesting de las piezas que conforman la silla, para poder determinar cuanto material entra en la Fabricación.

* Mostramos el nesting y prototipo realizado.

![](../images/MT09/3.png)

* Una vez que tenemos el archivo en **2D**, procedemos a grabar el archivo en dxf, para importar al programa **EASEL**
* En las imágenes indicamos los pasos que hemos seguido para poder generar ***G-CODE***

![](../images/MT09/1a.png)

![](../images/MT09/2b.png)

![](../images/MT09/3c.png)

* Una vez terminado los pasos citados anteriormente, exportamos el archivo de **G-CODE.**

## IV. ARCHIVOS EDITABLES

[archivodxf](../images/MT09/SILLAFINAL1.dxf)

[archivonc](../images/MT09/cgodesilla.nc)
