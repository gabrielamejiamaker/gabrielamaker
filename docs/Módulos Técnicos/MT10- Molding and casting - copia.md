---
hide:
    - toc
---

# MT 10

![](../images/MT10/caratula.png)

## I. BASE CONCEPTUAL

a.) **Definición de moldeado, fundición y procesos de Moldes**

![](../images/MT10/Diapositiva1.JPG)

b.) **Tipo de Casting**

![](../images/MT10/Diapositiva2.JPG)

c.) **Historia de los Moldes**

![](../images/MT10/Diapositiva3.JPG)

d.) **Aplicaciones de uso de los Moldes**

![](../images/MT10/Diapositiva4.JPG)

e.) **Tipos de Moldes**

![](../images/MT10/Diapositiva5.JPG)

![](../images/MT10/Diapositiva6.JPG)

f.) **3D MILLING**

![](../images/MT10/Diapositiva7.JPG)

g.) **Burbujas y Fallos**

![](../images/MT10/Diapositiva8.JPG)

h.) **Uso de Técnicas**

![](../images/MT10/Diapositiva9.JPG)

i.) **Tipos de Mezcla**
![](../images/MT10/Diapositiva10.JPG)

j.) **Diseño de Moldes**

![](../images/MT10/Diapositiva11.JPG)

![](../images/MT10/Diapositiva12.JPG)

![](../images/MT10/Diapositiva13.JPG)

k.) **Materiales para MILLING**

![](../images/MT10/Diapositiva14.JPG)

l.) **Moldes de gran tamaño y Resistencia**

![](../images/MT10/Diapositiva15.JPG)

m.) **Materiales para castear Flexiblemente**

![](../images/MT10/Diapositiva16.JPG)

![](../images/MT10/Diapositiva17.JPG)

n. ) **Materiales para castear casi Flexiblemente**

![](../images/MT10/Diapositiva18.JPG)

![](../images/MT10/Diapositiva19.JPG)

o.)** Materiales duros para casting.**

![](../images/MT10/Diapositiva20.JPG)

p.) **Fallos y Seguridad.**

![](../images/MT10/Diapositiva21.JPG)

![](../images/MT10/Diapositiva22.JPG)

## II. DESAFIO

  a.) Diseñar un modelo de maceta de 10cmdealto x15cm de diámetro

  **CONCEPTO:**
  El diseño de la Maceta esta basada en un elemento preinca de la cultura de mi País.

## III. DESARROLLO DEL DESAFIO

  Adjunto los planos de diseño, asi como el nesting de las piezas.

  ![](../images/MT10/a.png)

  ![](../images/MT10/5.jpg)

  ![](../images/MT10/2.png)

El corte de las piezas lo realizamos en láser.

1. Procederemos al armado de las piezas

2. Preparación del Yeso.

3. Vaciado del Yeso al Molde

4. Dejar el secado del molde

5. Desmoldar el molde.

![](../images/MT10/3.png)

![](../images/MT10/4.png)

**CONCLUSION:**

1. Como no pudimos hacer el molde en impresión 3d, cortamos las piezas en corte láser en cartón Corrugado.
2. Usamos un material como el yeso para poder hacer el molde

## IV. ARCHIVOS EDITABLES

[archivodxf](../images/MT10/maceta.dxf)
