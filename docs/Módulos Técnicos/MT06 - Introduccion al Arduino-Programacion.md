---
hide:
    - toc
---

# MT 06

![](../images/MT06/arduino.png)

## I. BASE CONCEPTUAL

a.) **ARDUINO1**

![](../images/MT06/Diapositiva1.JPG)

b.) **ARDUINO SHIELDS**

![](../images/MT06/Diapositiva2.JPG)

c.) **ARDUINO ECOSYSTEM**

![](../images/MT06/Diapositiva3.JPG)

d.) **ELECTRICIDAD**

![](../images/MT06/Diapositiva4.JPG)

e.) **ARDUINO PROGRAMMING**

![](../images/MT06/Diapositiva5.JPG)

f.) **PYSICAL COMPUTING**

![](../images/MT06/Diapositiva8.jpg)

g.) **ARDUINO PIN OUT**

![](../images/MT06/Diapositiva9.jpg)

![](../images/MT06/Diapositiva10.jpg)

h.) **ARDUINO INPUTS**

![](../images/MT06/Diapositiva6.JPG)

i.) **ARDUINO OUTPUTS**

![](../images/MT06/Diapositiva7.JPG)

## II. DESAFIO

  a.) Usar Mínimo dos INPUTS( recomendamos Sensor de Temperatura + Fotoresistencias)
  b.) Actuar Mínimo dos OUTPUTS ( recomendamos sermotor + motor dc con transistor)

## III. DESARROLLO DEL DESAFIO

  a.) Primero desarrollanos lo de los INPUT. Programamos en **ARDUINO** y también lo desarrollamos en **THINKERCAD**

![](../images/MT06/1.png)

b.) Segundo desarrolle el ejercicio de  Mínimo dos OUTPUTS ( recomendamos sermotor + motor dc con transistor)

Realizamos los ejercicios estan desarrollados en cada lámina. Primero programamos en **ARDUINO** y de ahí lo desarrollamos en **THINKERCAD**

  ![](../images/MT06/2.png)

  ![](../images/MT06/3.png)

## IV. ARCHIVOS EDITABLES

[archivoino](../images/MT06/motorpin.ino)

[archivoino](../images/MT06/ponteciometro.ino)

[archivoino](../images/MT06/SensorTemperatura.ino)

![](../images/MT06/20.png)

[archivobrd](../images/MT06/archivothinkercad.brd)
