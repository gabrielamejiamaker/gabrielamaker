---
hide:
    - toc
---

# MT 04

![](../images/MT04/modelado3d.jpg)

## I. BASE CONCEPTUAL

a.) **Diseño Asistido por computadora:** es el uso de computadores para ayudar en la creación,modificación, análisis u optimización de un diseño.

b.) **NURBS/MESH**

![](../images/MT04/Nurbs.png)

c.) **Texturas:** El modelo 3D de un objeto y su mapa de texturas. El primero emplea mallas triangulares para describir la geometría de la
superficie del objeto

d.) **3D Scultuping:**

e.) **Diseño Paramétrico:**

![](../images/MT04/Paramétrico.png)

f.) **Diseño Generativo:** no se trata de diseñar, se trata de diseñar el sistema que diseña

![](../images/MT04/generativo.JPG)

## II. DESAFIO

  a.) Diseñar y modelar en 3D un elemento que sea de utilidad para tu proyecto final en el software paramétrico FUSION 360.

  b.) El modelo debe poder imprimirse en 3D

  c.) Las piezas deben tener un tamaño máximo de 20X20X20 cm

## III. MODELADO

  a.) El modelado lo realize en el programa **RHINOCEROS**

  b.) Elegi modelar primero en **2D**, en el programa Rhinoceros la **Pata Lateral Derecha**, que formará parte del Escritorio que realizare en el proyecto final.

  El escritorio estará compuesto por 4 piezas:

    b.1. El tablero
    b.2. La Pata Lateral Derecha
    b.3. La Pata Lateral Izquierda
    b.4. La Pata Central
    b.5  La base del Escritorio

  c.) En el modelado considere los elementos electrónicos que voy a usar en mi proyecto final como: **los SENSORES** y **LUZ NEOPIXEL CIRCULAR DE 12 LUCES**

  ![](../images/MT04/MODELADO 2.png)

  d.) En el **modelado 3D**, use el programa **RHINOCEROS**, copie la pieza en 2D y cree una capa para cada elemento que conformarán la pata para asi asignarle a cada una el material que se fabricaran como:

  * La parte maciza que será fabricado en **TRYPLAY FENOLICO DE 18MM**
  * La frase **APRENDAMOS LOS NUMEROS**, y **las piezas en L** se fabricará en **TRYPLAY DE 6MM**
  * Las piezas circulares seran fabricadas en combinacion de
**TRYPLAY FENOLICO DE 18MM** y **TRYPLAY DE 6MM**

![](../images/MT04/capas.jpg)

e.) Mostramos el modelado en **2D** y **3D** de la Pata Lateral Derecha

* He considerado en el encastre X/Y, la tolerancia que se usa de acuerdo al espesor del material en este caso es de **TRYPLAY FENOLICO DE 18MM**, la tolerancia usada es 18.5mm
* En el encastre dentado, también hemos usado considerado la tolerancia usada es 18.5mm
* He considerado los **DOGBONES** de acuerdo a la fresa a usar que es 6.35mm, los DOGBONES son de 7mm, para que la fresa pueda cortar sin sufrir.

  ![](../images/MT04/MODELADOPATA.png)

## IV. ARCHIVOS EDITABLES

[archivodxf](../images/MT04/Parte Escritorio3d.dxf)

[archivo3dm](../images/MT04/Archivo para 3d-1.3dm)
