---
hide:
    - toc
---

# MT 08  

![](../images/MT08/milling.png)

## I. BASE CONCEPTUAL

**a.) PCB´S**

 Es una placa de Circuito Impreso

  **PCB** básicamente es un soporte físico en donde se instalan componentes electrónicos y eléctricos y se interconectan entre ellos.

 Estos componentes pueden ser: chips, condensadores, diodos, resistencias, conectores, etc.

 Para conectar cada elemento en una **PCB** utilizamos una serie de pistas conductoras de cobre extremadamente finas y que general un carril, conductor, como si de un cable se tratase.

 En los circuitos más sencillos, solamente tenemos pistas conductoras en una cara o las dos visibles de la **PCB**, pero en otros más completos tenemos pistas eléctricas e incluso componentes apilados en múltiples capas de ellas.

El soporte principal para estas pistas y componentes es una combinación de fibra de vidrio reforzada con materiales cerámicos, resinas, plástico y otros elementos no conductores. Aunque actualmente se están utilizado componentes como celuloide y pistas de pintura conductora para fabricar PCB flexibles.

La primera placa de circuito integrado se construyó en 1936 a mano por el ingeniero Paul Eisler para ser utilizada por una radio. A partir de ahí los procesos se automatizaron para su fabricación a gran escala, primero en radios, y luego en todo tipo de componentes.

1. Porque de **PCB´S**
Los **PCB´S** tienen un papel vital en la actualidad, ya que la tecnología mejora día a día.
Estas placas de circuito son la base de los productos electrónicos, ya que se utilizan en casi todos los dispositivos electrónicos.

![](../images/MT08/Diapositiva1.PNG)

**b.) Tipos de PCB´S**
  Detallamos los tipos de PCB´S

![](../images/MT08/Diapositiva2.PNG)

![](../images/MT08/Diapositiva3.PNG)

![](../images/MT08/Diapositiva4.PNG)

![](../images/MT08/Diapositiva5.PNG)

![](../images/MT08/Diapositiva6.PNG)

**c.) Diseños de Reglas de PCB´S**

![](../images/MT08/Diapositiva7.PNG)

**d.) Fabricación de PCB´S**

  Describiré los cuatro tipos de fabricación que existe.

![](../images/MT08/Diapositiva8.PNG)

**e.) Mileado de PCB´S**

  Describiré el proceso y componentes del mileado

![](../images/MT08/Diapositiva9.PNG)

**f.) SRM- 20CNC**

**g.) Servicio de Fabricación**

  Se cuenta con dos empresas que realizan este servicio

![](../images/MT08/Diapositiva10.PNG)

**h.) Soldar**

**i.) Softwares de Diseño PCB´S**

![](../images/MT08/Diapositiva15.PNG)

**j.) KiCad**

 El software más usado.

![](../images/MT08/Diapositiva16.PNG)

  **1. Elemento usan en KiCad**

  Para proceder el diseño de PCB´S, se usan librerías, se usan símbolos y huellas (footprint)

![](../images/MT08/Diapositiva17.PNG)

  **2. Diseño de PCB´S**
   Primero realizamos un diseño Esquemático y de ahi grabamos y procedemos a importar a PCB´S Layout, una vez terminada esta esta se exporta con una extensión SVG

    a. Esquemático

    b. PCB´S Layout

    c. PCB´S Export

![](../images/MT08/Diapositiva18.PNG)

**k.) Otros conceptos para el diseño de PCB´S**

![](../images/MT08/i36.jpg)

## II. DESAFIO

  a.) Reproduce la placa de ejemplo en Kicad

  b.) Extra: Diseñar una placa que te permita conectar un sensor a un arduino sin necesidad de un breadboard (diseña tu propio shield)

## III. DESARROLLO DEL DESAFIO

  Para este desafío

      1. **PRIMERA ETAPA:**

    a.) Instalar y Abrír el programa KiCad
    b.) Instalar las librerías que se usa en los Fab Labs

![](../images/MT08/imagen1.jpg)

![](../images/MT08/imagen2.jpg)

    2. **SEGUNDA ETAPA:**

    a.) Crear un proyecto en este desafío se llama Tutorial.
    b.) Realizar el Esquematico mediante el uso de simbolos.
    c.) Realizar la Conexion con cables
    d.) Colocar etiquetas que representan los voltajes 5v y GND

![](../images/MT08/imagen3.jpg)

![](../images/MT08/imagen4.jpg)

![](../images/MT08/imagen5.jpg)

![](../images/MT08/imagen6.jpg)

    3. **TERCERA ETAPA:**
    a.) Abrir el editor de PCB´S
    b.) Importar el Esquemático.
    c.) Ordenar los footprint y realizar el cableado

![](../images/MT08/imagen7.jpg)

![](../images/MT08/imagen8.jpg)

    4. **CUARTA ETAPA:**

    a.) El desarrollo final de la PCB´S

![](../images/MT08/imagen9.jpg)

## IV. ARCHIVOS EDITABLES

[archivopcb](../images/MT08/tutorial.kicad_pcb)

[archivosch](../images/MT08/tutorial.kicad_sch)
