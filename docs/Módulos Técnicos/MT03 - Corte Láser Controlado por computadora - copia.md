---
hide:
    - toc
---

# MT 03

![](../images/MT03/laser.jpg)

## I. BASE CONCEPTUAL

  a.) **CAD:** Es el diseño asistido por computadora

  b.) **CAM:** Es la fabricación asistida por computadora o fabricación asistida por ordenador

  c.) **Corte Láser:** Es una tecnología que funciona dirigiendo la salida de un láser de alta potencia través de la óptica. Es una forma rápida, fácil y precisa de cortar y grabar ilustraciones detalladas. El rayo láser enfocado se dirige al material, que luego se derrite, se quema, se evapora o es arrastrado por un chorro de gas, dejando un borde con un acabado superficial de alta calidad.

  d.) **Las Máquinas de Corte Láser:** funcionan básicamente como una impresora de inyección de tinta 2D convencional, pero vienen con controladores que permiten que la máquina de corte láser siga diseños específicos.
  Son de CO2 y FIBRA.

  e.) **Nesting:** Se usa antes de cortar para optimizar las piezas y reducir el desperdicio de material. La optimización se puede realizar de forma manual o mediante softwares de anidamiento.

  f.) **Kerf:** Se define como el ancho del material que se elimina mediante un proceso de corte.

  g.) **Tipos de Corte:**

    1.) **VECTOR:** Es el modo vectorial es el más utilizado. El láser seguirá todos los vectores dibujados siempre que su grosor sea igual a cero.

    2.) **RASTER:** En el modo de trama, el láser trata todo como una imagen. Suele utilizarse para grabar áreas sólidas, como texto, o para crear imágenes. Para lograr esto, el láser actuará como un cartucho de impresión en una impresora, "imprimiendo" gradualmente la obra de arte deseada.

  h.) **Operaciones de Corte**

  1.) **Corte vectorial:** Es cortando directamente a través del material. Se representa de color rojo

  2.) **Grabado raster** es el proceso estándar para grabar. Se representa de color negro

  3.) **Grabado vectorial o delineado:** Se encuentra a medio camino entre el corte vectorial y el grabado raster. En este método el láser sigue líneas vectoriales, pero solo corta en la superficie del material, es representado de color azul

## II. APRENDIZAJE PARA EL DISEÑO EN CORTE LASER

  a.) Bocetar lo que deseas fabricar

  b.) Puedes crear un diseño o patrón para corte con láser, usando programas de diseño 2D o 3D, en una lapto o PC

  c.) Procede a realizar el diseño en un Plano, tomando en cuenta el tipo de encastre y tolerancia en relación al espesor del material.

  d.) Creamos las capas para los diferentes grabados y corte, asignamos un color a cada capa.

    **ROJO: CORTE**

    **AZUL: DELINEADO O GRABADO VECTORIAL**

    **NEGRO: GRABADO O SCAN**

  e.) A realizar el nesting de las piezas que se va a cortar, tomando en consideraciones el largo por ancho del material a usuar.

  f.) Importar el **archivo dxf** en el programa para corte láser

  g.) Ajustar los **parámetros** que determinará la profundidad a la que cortará el láser:

    **Potencia: el porcentaje de potencia utilizada por el láser.**

    **Velocidad: la velocidad a la que se mueve el carro láser a lo largo de su trabajo.**

    **PPI:La tasa de disparo del láser a medida que se mueve (pulsos por pulgada)**

![](../images/MT03/Proceso Diseño.png)

## III. DESAFIO

  a.) **Consideraciones de Diseño**:

  b.) El Material a utilizar es MDF de 3mm

  c.) Dimensiones 600 x 450 mm.

  d.) El objeto de diseño debe tener como mínimo 3 piezas, ensamblar mediante encastres (sin la utilización de pegamento o fijaciones externas)

  e.) Aplicar en alguna de sus partes, la técnica de curvado de madera (kerf bending)

  f.) Contener  las 3 operaciones básicas de la máquina láser (grabado raster, marcado sobre vector y corte sobre vector)

## IV. PROCESO DE ELABORACION DE DISEÑO

  a.) El primer paso para realizar el ejercicio solicitado fue definir qué objetos realizaría.

  b.) Después de tanto pensar decidí realizar objetos que complementen a mi proyecto final y que puedan ser usados por el usuario.

  c.) Decidí diseñar un Kit de escritorio compuesto por: **Libreta de Apuntes, libreta de notas, porta lapices y porta posit.**

  d.) Busque referencias de este kit en plataforma como: Pinterest y google

  e.) Realice algunos bocetos

  f.) Dibuje en **Rhinoceros** los productos siguiendo las consideraciones solicitadas

  g.) Define como iba a realizar los **encastres**, para lo cual use dos tipos de encastres: el encastre x e y  y el encastre de enganche

  h.) Considerar la **tolerancia** necesaria para que la pieza pueda unirse con la otra o pueda ingresar en el canal planteado

![](../images/MT03/Planocorte.png)

  i.) Una vez que está el archivo listo para el Corte, se **exporta el archivo de Rhinoceros** se selecciono lo que se va cortar y se graba el archivo en ***formato DXF***.

  j.) Se **abre el programa de corte** que usa la máquina láser que es ***RDWORKS.***

  k.) Importa el archivo y una vez ya abierto se procede a borrar las doble líneas con ***DELETE OVERLAP***

  l.) Coloca **los parámetros para corte**: grabado vectorial o delineado y Grabado Raster o Scan y coloca el **orden en que se cortan.**

  m.) Si deseas ver como quedará tu archivo antes de cortar, puede hacer click en el icono tipo una pantalla, ahí podrás visualizar tu archivo y tambien saldra el tiempo que se demorara en cortar la máquina

![](../images/MT03/archivo rd.jpg)

  n.) Una vez que ya está listo se da *START* y el archivo saldrá en la pantalla de la máquina láser.

  o.) Se coloca el material en la cama de la máquina, se calibra la máquina, y de ahí se da FRAME para verificar si nuestro archivo va a entrar en el material que hemos colocado.

  p.) De ahí se coloca *START* y la máquina empezará a cortar y corta según el orden estipulado primero el Grabado de ahí el Delineado y por último el corte.

  q.) Cada cierto tiempo se va revisando nuestro corte, así como si está bien calibrada la máquina.

![](../images/MT03/Planocorte1.png)

  r.) Para realizar este ejercicio se envió primero un archivo a UTEC, ahí los docentes cortaron mis piezas y dieron algunas consideraciones que tuve que reajustar:

  ![](../images/MT03/Correpciones.jpg)

## IV. CORREPCIONES:

a.) La tolerancia del encastre del portalápices de animalitos NO estaba bien

b.) La base tenía muy pocos encastres para el tamaño.

c.) El número de diente de encastre en el portalápices era muy poco

**Tomando estas consideraciones realice nuevamente el archivo en lo que reajuste:**

a. Probé los encastres

b. Disminuir el tamaño de largo del portalápices de animalitos

c. Incluir en el kit la libreta post it y el porta taco de notas

d. Use diferentes anchos en el **kerfing.**

![](../images/MT03/cortearreglado.png){width="600"}

Anexamos la imagen donde se muestra el proceso de corte Láser de los objetos

![](../images/MT03/Corte.jpg){width="600"}

## V RESULTADO

  * Los productos obtenidos son el resultado del conocimiento teórico adquirido sobre corte láser

  * Antes de cortar todo el proyecto debe hacerse una prueba del grabado, delineado y corte para ver si es lo que uno desea, sino se reajusta los parametros

  * Una vez reajustado el archivo con las observaciones dada por los docentes me anime a cortar mis propias piezas, tome en cuenta las consideraciones de:

    1.Nunca dejar a la maquina sola

    2.Usar protección

    3.Verificar el extractor y la refrigeración

    **ARMAMOS LAS PIEZAS Y OBTUVIMOS COMO RESULTADO:**

    #**NUESTROS PRODUCTOS YA TERMINADOS**#

  ![](../images/MT03/Productos.jpg){width="600"}

## VI ARCHIVO EDITABLE

[archivodxf](../images/MT03/CortelaserMT03.dxf)
