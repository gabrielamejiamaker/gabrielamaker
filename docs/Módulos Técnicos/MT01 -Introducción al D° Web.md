---
hide:
    - toc
---

# MT 01

![](../images/MT01/web.jpg)

## I. BASE CONCEPTUAL

1. **Git**: Sus datos son como una serie de snapshot de un sistema de archivos en miniatura. Con Git, cada vez que confirma o guarda el estado de su proyecto. Git básicamente toma una imagen de cómo se ven todos sus archivos en ese
momento y almacena una referencia a esa snapshot.
Si los archivos no han cambiado, Git no almacena el archivo nuevamente, solo un enlace al archivo idéntico anterior que ya fue almacenado

Git tiene tres estados principales en los que pueden residir sus archivos: modified, staged, and committed:

    1. modified significa que ha cambiado el archivo pero aún no lo ha enviado a su base de datos.

    2. staged significa que ha marcado un archivo modificado en su versión actual para ir a su próxima snapshot de confirmación.

    3. committed significa que los datos se almacenan de forma segura en su base de datos local.

    Esto nos lleva a las tres secciones principales de un proyecto de Git:

    1. Árbol de trabajo (working directory

    2. El área de preparación (staging area)

    3. El directorio de Git (git directory).

![](../images/MT01/imagen1.JPG)

2. **Git Lab:**  Es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Es un de gestor de repositorios, el servicio ofrece también alojamiento de wikis.

  Un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.
  GitLab es una suite completa que permite gestionar, administrar, crear y conectar los repositorios con diferentes aplicaciones y hacer todo tipo de integraciones con ellas, ofreciendo un ambiente y una plataforma en cual se puede realizar las varias etapas de su SDLC/ADLC y DevOps.

3. **Mkdocs:** Es un generador de sitios estáticos rápido, simple que está orientado a la creación de documentación de proyectos. Los archivos de origen de la documentación se escriben en Markdown y se configuran con un solo archivo de configuración YAML

4. **Markdown:** Es un lenguaje que tiene la finalidad de crear contenido de una manera sencilla de escribir, y mantenga un diseño legible, así que para simplificar puedes considerar Markdown como un método de escritura

5. **Atom:** es un editor de código open source para Mac, Linux, y Windows​ con soporte para múltiples plug-in escritos en Node.js y control de versiones Git integrado, desarrollado por GitHub.
La mayor parte de los paquetes (plug-in) que se pueden instalar en Atom tienen licencias de software libre y están desarrollados y mantenidos por la comunidad de usuarios. Atom está basado en Electron (Anteriormente conocido como Atom Shell), un framework que permite crear aplicaciones de escritorio multiplataforma usando Chromium y Node.js

## II. DESAFIO

  1. Crear tu sitio web personal y documentar qué herramientas utilizaste.
  2. Trabajar con un tutorial de git.

  **2.1 EXPERIENCIA para crear mi página web.**

  * Al principio no fue buena tuve problemas para poder validar mi cuenta de la Universidad, eso me tardó en arreglar como un par de semanas.
  * De ahí con el asesoramiento de la instructora Carolina pude validar mi cuenta y de ahí poder validar mi cuenta de Git Lab
  * Durante el tiempo que estuve sin validar mi cuenta, estudié varios tutoriales de Atom, git y mkdocs.
  * En un primer momento esto me parecía como chino, poco a poco empecé a entender su funcionamiento
  * Una vez instalada y creada la página fue muy gratificante ver que funcionaba, como todo fue un proceso, seguire aprendiendo hasta que termine la especialización
  * Todo estaba funcionando y me abandono la tecnología mi laptop se malogro, tuve que trabajar en otra.
  * También se malogro el internet una semana, asi que dije como avanzo yo tengo todos mis archivos en mi drive, busque pasar internet desde mi móvil y bajar mis archivos para seguir avanzando.
  * Puse en practica un consejo de mi amigo programador que redacte todo en word y de ahí lo pase a ATOM, me fue más fácil, es rapido pero toma también su tiempo
  *Al final volvi a instalar ATOM, GitLab, Gitbush en mi laptop, una EMOCION todo funciono.

  A continuación voy a detallar los pasos para validar el Gitlab

![](../images/MT01/imagen2.JPG)

2.2 Proceso de Contenido y Edición

Una vez que esta ya la pagina creado, comenzamos a editar el contenido	Procedi a instalar ATOM y comencé a instalar paquetes que me ayudaran en la edición
  Los paquetes que instale son:

  * Emmet
  * Live Server
  * File Icons

![](../images/MT01/imagen3.JPG){width="600"}

  Linkeamos los archivos con el lenguaje Mkdocs.
	Aparecen los archivos en Atom, estan linkeados con el repositorio que está en la carpeta de mi pc.
  Las carpetas son:

    1. Gitlab-ci.yml

    2. Gitignore

    3. README, es un archivo que lee el repositorio de Git Lab

    4. Docs:información de la web

    5. Mkdocs.yml: archivo que configura la web

    6. Index.md: busca un archivo htlm

### III. APRENDIZAJES Y FALLOS

  3.1 **APRENDIZAJES:**

  * El nombre de los archivos son todas minúsculas o mayúsculas no colocar tildes.
  * Tener carpeta ordenadas, en el archivo docs
  * Las imágenes deben ir en carpetas deben estar relacionados con el título de cada módulo
  * Los files tiene que estar organizado por carpeta
  * Se empieza a ingresar la información en ATOM, es super importante grabar esta informacion de ahi SAVE ALL de ahí sale *stage all*, click, escribes un comentario de lo estas cambiando haces click en commit to master y de ahí click en Git push, vas a git lab y CC/CI vas Pipelines y verificas que se esta corriendo una vez que termina.
* Instalar paquetes en ATOM que ayudan al trabajo.
* Crear un repositorio paralelo al repositorio clonado con git en ese repositorio esta toda la información que se ha ido recopilando
* Redactar en una hoja de word en contendio que ira en ATOM.
* Ir a tu página refrescas y ves si aprarecen los cambios que has hecho
* Resaltar un texto en negrita se coloca la frase entre asterisco **modulo**
* Colocar una frase cursiva y negrita se coloca tres asteriscos ***innovación***
* Para anexar un link se coloca lo siguiente:

<a href=https://nombre de la pagina /" target="_blank">Enlace a otra web</a>

 3.2 **FALLOS**

  * Al insertar las imágenes estan eran muy grandes o pequeñas se investigo y se puedo calibrar esta dimenesiones.
