---
hide:
    - toc
---

# MT 02

![](../images/MT02/diseño2d.jpg)

## INTRODUCCIÓN AL DISEÑO 2D PARA LASER Y CNC

El diseño asistido por computadora es el uso de computadores para ayudar en la creación, modificación, análisis u optimización de un diseño.

### I. BASE CONCEPTUAL

a.) **Vector:** son la descripción geométrica (matemática) de una imagen.

b.) **Raster:** es la unidad mas pequeña de mapa bits.

![](../images/MT02/vector-raster.JPG){width="900"}


c.) **Espacio Color:** conformado por los espacios:

a.) SRGB   
b.) RGB  
c.) Adobe RGB   

d.) **Resolución:** es el producto del ancho y alto de una imagen digital        expresada en píxeles, lo que  Determina la calidad de la imagen es la densidad de píxeles que contiene.

1.) Para trabajos en Internet se recomienda 72-150 ppi

2.) Para trabajos de impresión se usa de 150-300 ppi

e. **Formatos de imágen y formatos Raster**
Dentro de los formatos tenemos los formatos:

1.) JPG                       
2.) PNG       
3.) TIFF    
4.) PSD  
5.) RAW

![](../images/MT02/Formato.JPG){width="1000"}

f. **Aplicaciones:**

1.) CNC        

2.) Corte Láser

3.) Ploter de Vínil

![](../images/MT02/Aplicaciones.JPG){width="1000"}

### II. DESAFIO

a.) **ISOTIPO VECTOR**

Para realizar el ejercico del ISOLOGO, explore el programa Rhinoceros, es un programa que te permite vectorizar a partir de imagenes o desde cero.
Para elaborar el isotipo, seguí los siguientes pasos:

  1. **PROCESO DEL ISOTIPO**

  1.1 **PRIMER PASO:** Busco referencias de imágenes que tengan relación con mi idea de PROYECTO que voy a desarrollar

![](../images/MT02/Referencia.JPG){width="600"}

  1.2 **SEGUNDO PASO:** Realice un boceto a mano alzada de la idea de isotipo, usando referencias las imágenes elegidas

![](../images/MT02/boceto2.jpg){width="800"}

  1.3 **TERCER PASO:** Este paso lo realice dos veces:
  mi primer isotipo no reflejaba la referencia de los isotipos de UTEC. Adjunta mi primer isotipo

![](../images/MT02/isotipo1.jpg){width="600"}

  Volví a replantear nuevamente mi isotipo. Busque otra imagen que reflejara la imagen de  niños, luego inserte las imágenes de referencia usando el comando PICTURE. Selecione las imágenes esten en una misma capa y coloque  candado a la capa para que no se mueva la imagen y pueda vectorizar.

  1.4 **CUARTO PASO:** Una vez fijadas las imágenes empiecé a dibujar con el comando CURVE las imágenes

  1.5 **QUINTO PASO:** Al terminar de vectorizar procedí a hacer HATCH al vector teniendo como referencia los isotipos de UTEC.

  1.6 **SEXTO PASO:** Hice HATCH al fondo del isotipo, y me base en el color usado en el objetidos de desarrollo planteado por el banco mundial.

![](../images/MT02/ProcesoIsotipo.JPG){width="700"}

  1.7 **SETIMO PASO:** Hice una captura de pantalla usando el comando VIEW CAPTURE FILE.

![](../images/MT02/isologo.jpg){width="600"}

b.) ***DIFICULTADES:***

  1. La dificultad que tuve es a nivel de concepto el diferenciar entre logo e isotipo.
  2. La primera imagen que realice no reflejaba el concepto de ISOTIPO.
  3. Se volvio a replantear el ISOTIPO y quede satisfecha ya que el isotipo reflejaba mi idea de proyecto.

c.) **IMAGEN RASTER**

Este ejercicio fue mas díficil, porque no uso programas para editar imágenes. Investigue las diferentes opciones propuestas por el programa.

1.) Gimp OpenSource

2.) Adobe Photoshop

3.) krite

4.) Pixlr Fremium

De los softwares propuestos, me decidí por usar el Pixlr Fremiun, es un software amigable y se trabaja directamente en el navegador.

d.) **PROCESO DE LA IMAGEN RASTER**

  1. **PRIMER PASO:** Busco referencias de imágenes que tengan relación con la idea de MI PROYECTO que voy a desarrollar

  2. **SEGUNDO PASO:** Abrir el programa Pixlr Fremium y crear una base de trabajo de 1080x 1080

  3. **TERCER PASO:** Inserte la  imagen de los niños y la coloco como fondo en una capa, de ahi bloquee la capa

  4. **CUARTO PASO:** Inserte la  imagen de mueble  y la coloco a un costado, esta imagen se quita el fondo que tenia que era blanco para eso se usa el comando vara de Seleción

e.) **DIFICULTADES:**

  1. La dificultad que tuve no conocía los programas de edición de imagenes

  2. Buscar las imagenes correctas que reflejen mi idea de proyecto.

![](../images/MT02/Proyecto1.jpg){width="500"}

## III. ARCHIVOS EDITABLES

[archivodxf](../images/MT02/isotipofinal1.dxf)
