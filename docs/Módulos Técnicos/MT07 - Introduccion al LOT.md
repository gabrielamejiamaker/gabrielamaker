---
hide:
    - toc
---

# MT 07

![](../images/MT07/lot.png)

## I. BASE CONCEPTUAL

a.) **INTRODUCCIÓN**

1. El uso de redes y protocolos de comunicación se basa en la idea de distribuir y conectar sistemas por motivos de:

![](../images/MT07/imagen1.JPG)


b.) **PROTOCOLOS vs REDES**

1. Una **red:** Es un conjunto de ordenadores conectados entre sí a través de líneas de comunicación.

2. Los **protocolos:** Son conjuntos de normas para formatos de mensaje y procedimientos que permiten a las máquinas y los programas de aplicación intercambiar información.


c.) **Tipos de Redes:** Las redes se clasifican:

![](../images/MT07/imagen2.JPG)

1. Buscamos conseguir comunicar dos ordenadores. Usaremos redes y protocolos de comunicación sencillos implementados ya en librerías de Arduino.
2. En función de nuestro tipo de red, y propósito, usaremos un protocolo u otro de comunicación

![](../images/MT07/imagen3.JPG)


d.) **Comunicación por CABLE:** implican que el medio físico de transmisión de información se realiza por uno o más cables. Se pueden clasificar en:

![](../images/MT07/imagen4.jpg)

e.) **Comunicación por SIN CABLE:** Nuestra capacidad de modular el espectro acústico o electromagnético y codificar información en él. Normalmente, la frecuencia de la onda que se usa para la comunicación es el factor más importante de cara a su implementación y clasificación:


f.) **Serial Arduino:**

  1. Tiene posibilidad de comunicarse con ellas a través de la librería Serial

  2. Se usa para la comunicación entre la placa de Arduino y otros dispositivos, entre ellos nuestro ordenador. Todas las placas tienen al menos un puerto.

  3. Para usarla, simplemente es necesario abrir el puerto Serial con:

![](../images/MT07/imagen7.jpg)


  4. Teniendo en cuenta que el baudrate no puede ser cualquiera. Luego podemos enviar algo:


![](../images/MT07/imagen8.jpg)

  5. En el otro extremo, deberemos usar la misma velocidad (9600 en el caso de arriba) y podremos abrir la comunicación.

  6. En el Arduino IDE, existe un Monitor Serial que nos permite leer lo que recibimos, aunque existen otros muchas formas de abrir el puerto (debajo usaremos Processing):

g.) **Funciones en Arduino**

  Detallare en el siguiente cuadro las funciones, asi como el detalle para que sirven y el su codigo para programar en arduino, los cuales seran usados en el Desafio que voy a realizar mas adelante

![](../images/MT07/imagen18.jpg)

h.) **MQTT:**

![](../images/MT07/imagen5.jpg)

![](../images/MT07/imagen6.jpg)

i.) **Processing**

  Es un lenguaje de programación para aprender a programar en el contexto de artes visuales.
  Lo usaremos para recibir información del Arduino por Serial (USB) y enviarlo a otro Arduino a través de un protocolo llamado MQTT.

j.) **Ponteciometro**

k.) **Servo Motor**

## II. DESAFIO

  a.) Cada alumno debe publicar la lectura de un sensor y usar la publicación de alguno de sus compañeros para controlar su actuador.

  b.) Comunicación entre dos dispositivos por cable USB usando protocolo Serial

  c.) Comunicación entre dos o mas dispositivos remotos utilizando el protocolo MQTT

  ![](../images/MT07/imagen20.jpg)

## III. DESARROLLO DEL DESAFIO

  Para este desafío primero conformamos equipos, yo trabaje con Robert y con Carlos

  Para empezar el desafío:

    1. **PRIMERA ETAPA:**

  a.) Descargar e instalar los programas:

    1. Arduino, adjunto el link

      https://www.arduino.cc/en/software

    2. Descargar e instalar el programa Processing, adjunto el link:
      https://processing.org/download

  b.) Revisamos el documento de referencia proporcionado por la universidad.

  c.) Yo voy a realizar lo del Led, para lo cual buscamos los elementos que vamos a necesitar:

    1. La resistencia, mediante la Calculadora de código de colores de resistencias de 4 bandas, calculamos la resistencia es de 220 ohms

![](../images/MT07/imagen11.jpg)

    2. El led, la placa de arduino y la placa plateada.

![](../images/MT07/imagen21.jpg)

  d.) Armamos los componentes según la referencia de la ayuda memoria.

  e.) Para certificar estaba bien, el circuito lo desarrollamos usando el programa de THINKERCAD, y ahi obtuvimos el codigo para Arduino

![](../images/MT07/imagen9.jpg)

  f.) Una vez que verficamos que lo fisico y lo realizado en THINKERCAD esta bien ambos, procedimos abri el programa de arduino.

  g.) Al principio tuvimos errores en la programación teniamos doble void setup y doble void lool

![](../images/MT07/imagen10.jpg)

  h.) Procedi a realizar de nuevo el codigo en ARDUINO y funciono

  ![](../images/MT07/imagen12.jpg)  

  ![](../images/MT07/imagen13.jpg)

  g.) Abrimos el programa processing, realizo el codigo y se dio play. Cuando realizo el ejercicio con el docente Victor el Led prendio, realice el ejercicio con un miembro de mi equipo Robert no prendio, tenemos que descubir en donde esta el error

![](../images/MT07/imagen14p.jpg)   ![](../images/MT07/imagen15p.jpg)

![](../images/MT07/imagen16p.jpg)   ![](../images/MT07/imagen17p.jpg)

    2. **SEGUNDA ETAPA:**

    a.) Funcionamiento del Led

![](../images/MT07/imagen22.jpg)  ![](../images/MT07/imagen23.jpg)
