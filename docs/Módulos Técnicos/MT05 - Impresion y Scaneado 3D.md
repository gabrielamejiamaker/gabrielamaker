---
hide:
    - toc
---

# MT 05

![](../images/MT05/impresiona.jpg)

## I. BASE CONCEPTUAL

a.) **IMPRESIÓN 3D**

a.1. **Historia de la Impresión 3D**
Detallaremos un recorrido de la historia desde la aparición de la primera impresora 3D hasta la actualidad

a.2. **Principios de la impresión**
Está conformado por los aspectos más relevantes de esta tecnología

![](../images/MT05/imagen1.JPG)

a.3. **Tipos de Impresión 3D:** Apreciamos que existen diferentes tipos de impresión, los cuales también usan diferentes medios donde se imprimen como el agua, el aire asi mismo los diferentes materiales a usar para imprimir como: filamento PLA, concreto, comida, mortero, adobe, entre otros.

![](../images/MT05/imagen2.JPG)

a.4.  **Tecnologias de Impresion 3D:** Explicamos las tecnologías más representativas

![](../images/MT05/imagen4.JPG)

a.5. **Filamentos más usados para Impresión 3D:** Tenemos 3 tipos de filamentos que son los más usados y detallamos las características de cada uno

![](../images/MT05/imagen3.JPG)

a.6. **Preparación del Archivo para Impresión 3D:** Indicaremos los pasos que tenemos que tomar en cuenta cuando elaboramos un archivos para impresión 3d

![](../images/MT05/imagen5.JPG)

a.7. **Consideraciones:** Están indicadas las diferentes consideraciones que se tiene que realizar en el programa CURA para poder imprimir nuestro archivo

![](../images/MT05/imagen6.JPG)

a.8. **Formato de Archivo:** Colocamos los formatos más usados así como las ventajas y desventajas de su uso

![](../images/MT05/imagen7.JPG)

b.) **SCANNEO 3D**

b.1. **Fotogrametría y tipos de Scaneo 3D:** Indicamos los aspectos que se consideran para realizar un Scaneo 3D, así como los diferentes tipos de scaneo usados en las diferentes industrias por ejemplo: agrícola

![](../images/MT05/imagen8.JPG)

b.2. **Programas y Plataformas:** Detallaremos los programas más usados para Scaneo 3d y una plataforma online para trabajar archivos escaneados.

![](../images/MT05/imagen9.JPG)

## II. DESAFIO

a.) Es la producción de una pieza en un equipo de FDM por lo que el modelo debe entregarse en código G y en STL siguiendo las referencias detalladas a continuación.

b.) Usar el Software para SLICING - CURA y el equipo para el que se realiza el Slicing - Ultimaker 3El modelo debe poder imprimirse en 3D

c.) Las Medidas 60x60x60mm máximo (es una prueba de impresión)

d.) Variables a definir por parte del estudiante:

  Altura de capa, densidad de relleno, soporte, material, ventilación, velocidad, posición del objetoLas piezas deben tener un tamaño máximo de 20X20X20 cm

## III. DESARROLLO

Para este desafío según las dimensiones solicitadas, se evaluó imprimir la patita de la lámpara que se va usar como parte de nuestro mueble

Para empezar a digitalizar la pata:

    1. **PRIMERA ETAPA:**

a.) Abrí el programas Rhinoceros

b.) Inserte un imagen de pata de lampara usando el comando Picture

c.) Use el comando curve para dibujar la patita

d.) En la vista perspectiva use el Comando Revolve y modele la pata

e.) En la capa donde esta modelado se agrego el material para visualizar la imagen

f.) guardar el archivo en la extensión stl

![](../images/MT05/proceso.JPG)

    2. **SEGUNDA ETAPA:**

a.) Descargue el programa Cura.

b.) Abrir el archivo stl

c.) Primer inconveniente que tuve salio muy pequeño el modelo de la patita y procedi a escalar la patita

d.) Procedí a colocar los parámetros en los siguientes ítem:

      * Ajustes agrego el tipo de impresora
      * Verificó que tenga un diámetro de 1,75mm y una boquilla de 0,4m
      * Altura de Capa de 0.2
      * Relleno tiene ser en un 10% no sea muy denso
      * Patrón de relleno: Rejilla
      * Material con impresion: temperatura de 205, flujo de 100, habilidad de detracción 5 mm  
      * Velocidad de Impresión: 50-60
      * Refrigeracion al 100%
      * Soporte distancia en z del 0.15 tipo árbol
      * Densidad del Soporte 20 y ángulo de 45 grados
      * Adherencia de 15m

![](../images/MT05/Impresion.JPG)

e.) Procede a dar click en el comando que calculara el tiempo de impresión.

f.) Guardar el archivo para impresión en Código G

g.) En los laboratorios de UTEC se imprimio la pieza.

## IV. ARCHIVOS EDITABLES.

[archivo3dm](../images/MT05/lampara3d.3dm)

[archivostl](../images/MT05/lampara3d.stl)

[archivo3d](../images/MT05/UM3_lampara3d.gcode)
